package com.challenge.glady.Helper;

import com.challenge.glady.Core.Entity.Company;
import com.challenge.glady.Core.Entity.User;
import com.challenge.glady.Data.Repository.DepositRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * helper for services which need to save deposits.
 */
@Service
public class DepositHelper {

    /**
     * The {@link DepositRepository} repository.
     */
    private final DepositRepository depositRepository;

    @Autowired
    public DepositHelper(DepositRepository depositRepository) {
        this.depositRepository = depositRepository;
    }

    /**
     * Registers the deposits of a user.
     *
     * @param user The user to manipulate.
     */
    public void registerDepositsForUser(User user) {
        depositRepository.saveAll(user.getDeposits());
    }

    /**
     * Registers the deposits of a company.
     *
     * @param company The company to manipulate.
     */
    public void registerDepositsForCompany(Company company) {
        depositRepository.saveAll(company.getDeposits());
    }
}
