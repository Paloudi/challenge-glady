package com.challenge.glady.Data.Repository;

import com.challenge.glady.Core.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository interface for {@link User} entities. It provides CRUD operations and extends JpaRepository
 * to inherit methods default methods.
 * This repository will be implemented by Spring's proxy mechanism at runtime.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Find a user by its username.
     *
     * @param username The username to use.
     * @return User if found, else null.
     */
    User findByUsername(String username);

    /**
     * Delete a user by its username.
     *
     * @param username The username to use.
     */
    void deleteByUsername(String username);

    /**
     * Search a user by its username.
     *
     * @param username The username to use.
     * @return True if a user exists with this username.
     */
    boolean existsByUsername(String username);
}
