package com.challenge.glady.Data.Repository;

import com.challenge.glady.Core.Entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository interface for {@link Company} entities. It provides CRUD operations and extends JpaRepository
 * to inherit methods default methods.
 * This repository will be implemented by Spring's proxy mechanism at runtime.
 */
@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {
    /**
     * Method used to find an existing company using its name.
     *
     * @param name The name of the company we're looking for.
     * @return The company.
     */
    Company findByName(String name);

    /**
     * Method used to delete an existing company using its name.
     *
     * @param name The name of the company to delete.
     */
    void deleteByName(String name);

    /**
     * Method used to check if a company exist using its name.
     *
     * @param name The name of the company to check.
     * @return True if the company exists.
     */
    boolean existsByName(String name);
}
