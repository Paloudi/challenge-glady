package com.challenge.glady.Data.Repository;

import com.challenge.glady.Core.Entity.Company;
import com.challenge.glady.Core.Entity.Deposit;
import com.challenge.glady.Core.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * Repository interface for {@link Deposit} entities. It provides CRUD operations and extends JpaRepository
 * to inherit methods default methods.
 * This repository will be implemented by Spring's proxy mechanism at runtime.
 */
@Repository
public interface DepositRepository extends JpaRepository<Deposit, Long> {
    /**
     * Fetches all deposits associated with a specific user.
     *
     * @param user The user for whom to fetch the deposits.
     * @return A list of deposits associated with the given user.
     */
    List<Deposit> findAllByUser(User user);

    /**
     * Fetches all deposits associated with a specified company.
     *
     * @param company The company for whom to fetch the deposits.
     * @return A list of deposits associated with the given company.
     */
    List<Deposit> findAllByCompany(Company company);

    /**
     * Fetch the deposit corresponding to the trio company, user and distributionDate.
     *
     * @param company          The company which distributed the deposit.
     * @param user             The user who received the deposit.
     * @param distributionDate The date of the deposit.
     * @return The deposit corresponding, or null.
     */
    @Query("FROM Deposit d WHERE TYPE(d) = :depositType AND d.company = :company AND d.user = :user AND d" +
            ".distributionDate = :distributionDate")
    Deposit findDepositByTypeAndCompanyAndUserAndDistributionDate(
            @Param("depositType") Class<? extends Deposit> depositType,
            @Param("company") Company company,
            @Param("user") User user,
            @Param("distributionDate") LocalDate distributionDate
    );
}
