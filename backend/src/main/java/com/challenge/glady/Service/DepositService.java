package com.challenge.glady.Service;

import com.challenge.glady.Core.Entity.Company;
import com.challenge.glady.Core.Entity.Deposit;
import com.challenge.glady.Core.Entity.User;
import com.challenge.glady.Core.Enum.DepositType;
import com.challenge.glady.Core.Factory.GiftDepositFactory;
import com.challenge.glady.Core.Factory.IDepositFactory;
import com.challenge.glady.Core.Factory.MealDepositFactory;
import com.challenge.glady.Core.Interface.IEntity;
import com.challenge.glady.Data.Repository.DepositRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service class for handling business logic related to {@link Deposit} entities.
 * This service directly interact with the {@link DepositRepository} for DB operations.
 */
@Service
public class DepositService implements IEntity {

    /**
     * The {@link DepositRepository} repository.
     */
    private final DepositRepository depositRepository;

    /**
     * The {@link CompanyService} service.
     */
    private final CompanyService companyService;

    /**
     * The {@link UserService} service.
     */
    private final UserService userService;

    /**
     * Map used for the deposit factory mapping between enum and deposit type.
     */
    private final Map<DepositType, IDepositFactory> depositFactoryMap;

    /**
     * Create a DepositService for {@link Deposit} manipulation.
     *
     * @param depositRepository The deposit repository.
     * @param companyService    the company service.
     * @param userService       the user service.
     */
    @Autowired
    public DepositService(DepositRepository depositRepository, CompanyService companyService, UserService userService) {
        this.depositRepository = depositRepository;
        this.companyService = companyService;
        this.userService = userService;
        // Deposit Factory
        depositFactoryMap = new HashMap<>();
        depositFactoryMap.put(DepositType.GIFT, new GiftDepositFactory());
        depositFactoryMap.put(DepositType.MEAL, new MealDepositFactory());
    }

    /**
     * Get a deposit from its id.
     *
     * @param id The id of the deposit.
     * @return The deposit or null if not found.
     */
    public Deposit getById(String id) {
        Deposit result = null;
        try {
            result = depositRepository.findById(Long.parseLong(id)).orElse(null);
        } catch (Exception e) {
            getLogger().error("Could not parse deposit id", e);
        }
        return result;
    }

    /**
     * Get the list of all the deposits stored in repository.
     *
     * @return A list of {@link Deposit} object.
     */
    public List<Deposit> getAllDeposits() {
        return depositRepository.findAll();
    }

    /**
     * Get the list of all the deposits of a company.
     *
     * @param name The name of the company.
     * @return A list of {@link Deposit} object.
     */
    public List<Deposit> getAllDepositsOfCompany(String name) {
        Company company = companyService.getByName(name);
        if (null != company) {
            getLogger().info("Getting deposits of company {}", name);
            return depositRepository.findAllByCompany(company);
        } else {
            getLogger().warn("Tried to get deposits of a non existing company {}", name);
            return new ArrayList<>();
        }
    }

    /**
     * Get the list of all the deposits of a user.
     *
     * @param name the name of the user.
     * @return A list of {@link Deposit} object.
     */
    public List<Deposit> getAllDepositsOfUser(String name) {
        User user = userService.getByName(name);
        if (null != user) {
            getLogger().info("Getting deposits of user {}", name);
            return depositRepository.findAllByUser(user);
        } else {
            getLogger().warn("Tried to get deposits of a non existing user {}", name);
            return new ArrayList<>();
        }
    }

    /**
     * Distributes a deposit from a company to a user.
     *
     * @param company The distributing company.
     * @param user    The receiving user.
     * @param amount  The amount to distribute.
     * @return A boolean indicator. True if the distribution succeeded, else false.
     */
    @Transactional
    public boolean distributeDeposit(DepositType depositType, Company company, User user, double amount) {
        boolean returnValue = false;
        CheckDepositDataValidity(company, user, amount);
        try {
            if (companyService.checkCompanyBalance(company, amount)) {
                company.setBalance(company.getBalance() - amount);

                IDepositFactory factory = depositFactoryMap.get(depositType);
                if (factory == null) {
                    throw new IllegalArgumentException("Unsupported deposit type: " + depositType);
                }
                Deposit deposit = factory.createDeposit(amount, LocalDate.now(), company, user);
                company.getDeposits().add(deposit);
                user.getDeposits().add(deposit);
                companyService.update(company);
                userService.update(user);
                depositRepository.save(deposit);
                depositRepository.flush();
                returnValue = true;
                getLogger().info("Creation of deposit {} for company {} and user {}", deposit.getId(),
                        company.getName(), user.getUsername());
            } else {
                getLogger().warn("Company {} does not have sufficient balance to distribute deposit.",
                        company.getName());
            }
        } catch (Exception e) {
            getLogger().error("Error distributing gift deposit from company {} to user {}", company.getName(),
                    user.getUsername(), e);
        }
        return returnValue;
    }

    /**
     * Fetches all valid (non-expired) deposits linked to a user.
     *
     * @param user The user.
     * @return A list of valid deposits.
     */
    public List<Deposit> getValidDepositsForUser(User user) {
        try {
            List<Deposit> allDeposits = depositRepository.findAllByUser(user);
            return allDeposits.stream().filter(deposit -> !deposit.isExpired()).collect(Collectors.toList());
        } catch (Exception e) {
            getLogger().error("Error fetching valid deposits for user {}", user.getUsername(), e);
            return Collections.emptyList();
        }
    }

    /**
     * Create a new meal deposit in the database.
     *
     * @param companyName The name of the company doing the deposit.
     * @param username    The name of the user receiving the deposit.
     * @param amount      The amount of the deposit.
     * @return True if the operation was a success, else false.
     */
    @Transactional
    public boolean createMealDeposit(String companyName, String username, double amount) {
        Company company = companyService.getByName(companyName);
        User user = userService.getByName(username);
        return distributeDeposit(DepositType.MEAL, company, user, amount);
    }

    /**
     * Create a new gift deposit in the database.
     *
     * @param companyName The name of the company doing the deposit.
     * @param username    The name of the user receiving the deposit.
     * @param amount      The amount of the deposit.
     * @return True if the operation was a success, else false.
     */
    @Transactional
    public boolean createGiftDeposit(String companyName, String username, double amount) {
        Company company = companyService.getByName(companyName);
        User user = userService.getByName(username);
        return distributeDeposit(DepositType.GIFT, company, user, amount);
    }

    /**
     * Update a deposit in the database.
     *
     * @param deposit The deposit to update.
     * @return The updated deposit, else null.
     */
    @Transactional
    public Deposit update(Deposit deposit) {
        Deposit result = depositRepository.findDepositByTypeAndCompanyAndUserAndDistributionDate(
                deposit.getClass(), deposit.getCompany(), deposit.getUser(), deposit.getDistributionDate());
        if (null != result) {
            result.setCompany(deposit.getCompany());
            result.setUser(deposit.getUser());
            result.setDistributionDate(deposit.getDistributionDate());
            result.setAmount(deposit.getAmount());
            result = depositRepository.save(result);
            getLogger().info("Update of of deposit {}/{}/{}",
                    deposit.getCompany(), deposit.getUser(), deposit.getDistributionDate());
        } else {
            getLogger().error("Update of a non existing deposit {}/{}/{}",
                    deposit.getCompany(), deposit.getUser(), deposit.getDistributionDate());
        }
        return result;
    }

    /**
     * Check if the given data are valid and can be processed.
     *
     * @param company The company to check.
     * @param user    The user to check.
     * @param amount  The amount to check.
     */
    private void CheckDepositDataValidity(Company company, User user, double amount) {
        if (null == company) {
            getLogger().error("Company can't be null");
            throw new NullPointerException("Company can't be null");
        }

        if (null == user) {
            getLogger().error("User can't be null");
            throw new NullPointerException("User can't be null");
        }

        if (Double.isNaN(amount)) {
            getLogger().error("Amount is not a number");
            throw new NullPointerException("Amount is not a number");
        }
    }

    /**
     * Calculates and returns the balance of the user by summing up the valid (non-expired) deposits linked to the user.
     *
     * @param user The user for whom to calculate the balance.
     * @return The balance of the user.
     */
    @Transactional
    public double getUserBalance(User user) {
        try {
            List<Deposit> validDeposits = getValidDepositsForUser(user);
            return validDeposits.stream().mapToDouble(Deposit::getAmount).sum();
        } catch (Exception e) {
            getLogger().error("Error calculating balance for user: {}", user.getUsername(), e);
            return Double.NaN;
        }
    }

    /**
     * Delete a deposit by its id.
     *
     * @param id the id of the deposit to delete.
     */
    @Transactional
    public boolean delete(String id) {
        boolean result = false;
        try {
            long idAsDouble = Long.parseLong(id);
            depositRepository.deleteById(idAsDouble);
            result = true;
        } catch (Exception e) {
            getLogger().error("Error deleting deposit {}", id);
        }
        return result;
    }
}
