package com.challenge.glady.Service;

import com.challenge.glady.Core.Entity.Company;
import com.challenge.glady.Core.Entity.Deposit;
import com.challenge.glady.Core.Interface.IEntity;
import com.challenge.glady.Data.Repository.CompanyRepository;
import com.challenge.glady.Helper.DepositHelper;
import jakarta.annotation.Nullable;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service class for handling business logic related to {@link Company} entities.
 * This service directly interact with the {@link CompanyRepository} for DB operations.
 */
@Service
public class CompanyService implements IEntity {

    /**
     * The {@link CompanyRepository} repository.
     */
    private final CompanyRepository companyRepository;

    /**
     * The {@link DepositHelper} helper.
     */
    private final DepositHelper depositHelper;

    /**
     * Create a CompanyService for {@link Company} manipulation.
     *
     * @param companyRepository The company repository.
     */
    @Autowired
    public CompanyService(CompanyRepository companyRepository, DepositHelper depositHelper) {
        this.companyRepository = companyRepository;
        this.depositHelper = depositHelper;
    }

    /**
     * Returns the list of all companies currently stored.
     *
     * @return A list of {@link Company} objects.
     */
    @Transactional
    public List<Company> getAllCompanies() {
        return companyRepository.findAll();
    }

    /**
     * Get a company by its name.
     *
     * @param name The name of the company.
     * @return The company if found, else null.
     */
    @Transactional
    public Company getByName(String name) {
        return companyRepository.findByName(name);
    }

    /**
     * Check if a company hav more balance than the given amount.
     *
     * @param company The company to check.
     * @param amount  The amount to check.
     * @return True is the company has enough balance, else false.
     */
    @Transactional
    public boolean checkCompanyBalance(@Nullable Company company, double amount) {
        if (company == null) {
            getLogger().error("Company is null");
            throw new NullPointerException("Company can't be null");
        }

        if (Double.isNaN(amount)) {
            getLogger().error("Amount is not a number");
            throw new NullPointerException("Amount must be a number");
        }

        boolean hasSufficientBalance = company.getBalance() >= amount;
        if (!hasSufficientBalance) {
            getLogger().warn("Company {} does not have sufficient balance. Current balance: {}, Required: {}",
                    company.getName(), company.getBalance(), amount);
        }

        return hasSufficientBalance;
    }

    /**
     * Create a new company in the database.
     *
     * @param company The company to insert.
     * @return The created company, else null.
     */
    @Transactional
    public Company create(Company company) {
        Company result = companyRepository.findByName(company.getName());
        if (null == result) {
            result = companyRepository.save(company);
            getLogger().info("Insertion of company {}", company.getName());
        } else {
            result = null;
            getLogger().error("Insertion of an already existing company");
        }
        return result;
    }

    /**
     * Update a company in the database.
     *
     * @param company The company to update.
     * @return The updated company, else null.
     */
    @Transactional
    public Company update(Company company) {
        Company result = companyRepository.findByName(company.getName());
        if (null != result) {
            if (company.getDeposits() != result.getDeposits()) {
                result.getDeposits().addAll(company.getDeposits());
                for (Deposit deposit : company.getDeposits()) {
                    deposit.setCompany(result);
                }
                depositHelper.registerDepositsForCompany(result);
            }
            result.setBalance(company.getBalance());
            result = companyRepository.saveAndFlush(result);
            getLogger().info("Update of company {}", company.getName());
        } else {
            getLogger().error("Update of an already existing company {}", company.getName());
        }
        return result;
    }

    /**
     * Delete a company given its name.
     *
     * @param name The name of the company to delete.
     * @return True if not found in database after operation.
     */
    @Transactional
    public boolean delete(String name) {
        companyRepository.deleteByName(name);
        return !companyRepository.existsByName(name);
    }
}
