package com.challenge.glady.Service;

import com.challenge.glady.Core.Entity.Deposit;
import com.challenge.glady.Core.Entity.User;
import com.challenge.glady.Core.Interface.IEntity;
import com.challenge.glady.Data.Repository.UserRepository;
import com.challenge.glady.Helper.DepositHelper;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service class for handling business logic related to {@link User} entities.
 * This service directly interact with the {@link UserRepository} for DB operations.
 */
@Service
public class UserService implements IEntity {

    /**
     * The {@link UserRepository} repository.
     */
    private final UserRepository userRepository;

    /**
     * The {@link DepositHelper} helper.
     */
    private final DepositHelper depositHelper;

    /**
     * Create a UserService for {@link User} manipulation.
     *
     * @param userRepository The User repository.
     */
    @Autowired
    public UserService(UserRepository userRepository, DepositHelper depositHelper) {
        this.userRepository = userRepository;
        this.depositHelper = depositHelper;
    }

    /**
     * Retrieves all users from the database.
     *
     * @return A list of all users.
     */
    @Transactional
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    /**
     * Get a user by its name.
     *
     * @param name The name to use.
     * @return The user if found, else null.
     */
    @Transactional
    public User getByName(String name) {
        return userRepository.findByUsername(name);
    }

    /**
     * Create a new user in the database.
     *
     * @param user The user to insert.
     * @return The created user, else null.
     */
    @Transactional
    public User create(User user) {
        User result = userRepository.findByUsername(user.getUsername());
        if (null == result) {
            result = userRepository.save(user);
            getLogger().info("Insertion of user {}", user.getUsername());
        } else {
            result = null;
            getLogger().error("Insertion of an already existing user {}", user.getUsername());
        }
        return result;
    }

    /**
     * Update a user in the database.
     *
     * @param user The user to update.
     * @return The updated user, else null.
     */
    @Transactional
    public User update(User user) {
        User result = userRepository.findByUsername(user.getUsername());
        if (null != result) {
            if (user.getDeposits() != result.getDeposits()) {
                result.getDeposits().addAll(user.getDeposits());
                for (Deposit deposit : user.getDeposits()) {
                    deposit.setUser(result);
                }
                depositHelper.registerDepositsForUser(result);
            }

            result = userRepository.saveAndFlush(result);
            getLogger().info("Update of user {}", user.getUsername());
        } else {
            getLogger().error("Update of a non existing user {}", user.getUsername());
        }
        return result;
    }

    /**
     * Delete a user given its name.
     *
     * @param name The name of the user to delete.
     * @return True if not found in database after operation.
     */
    @Transactional
    public boolean delete(String name) {
        userRepository.deleteByUsername(name);
        return !userRepository.existsByUsername(name);
    }
}
