package com.challenge.glady.Controller;

import com.challenge.glady.Core.Entity.Deposit;
import com.challenge.glady.Core.Entity.User;
import com.challenge.glady.Core.Interface.IEntity;
import com.challenge.glady.Service.DepositService;
import com.challenge.glady.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * The DepositController is a RESTful controller responsible for managing operations related to Deposit entities.
 */
@RestController
@RequestMapping("/api/deposit")
public class DepositController implements IEntity {

    /**
     * The {@link DepositService} service.
     */
    private final DepositService depositService;

    /**
     * The {@link UserService} service.
     */
    private final UserService userService;

    /**
     * Create a new DepositController.
     *
     * @param depositService The service used by this controller.
     */
    @Autowired
    public DepositController(DepositService depositService, UserService userService) {
        getLogger().info("Deposit Controller created");
        this.depositService = depositService;
        this.userService = userService;
    }

    /**
     * Get a deposit from an id.
     * @param id The id to use.
     * @return The deposit found or INTERNAL_SERVER8_ERROR if not found.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Deposit> getDepositById(@PathVariable String id) {
        getLogger().trace("getDepositById called with parameter {}", id);

        Deposit deposit = null;
        try {
            deposit = depositService.getById(id);
        } catch (Exception e) {
            getLogger().error("getDepositById exception caught", e);
        }

        if (null != deposit) {
            getLogger().trace("getDepositById finished");
            return new ResponseEntity<>(deposit, HttpStatus.OK);
        } else {
            getLogger().warn("getDepositById failed with parameter {}", id);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get the list of all the deposits.
     *
     * @return A list of {@link Deposit} object.
     */
    @GetMapping
    public ResponseEntity<List<Deposit>> getAllDeposits() {
        getLogger().trace("GetAllDeposits called");

        List<Deposit> result;
        try {
            result = depositService.getAllDeposits();
        } catch (Exception e) {
            getLogger().error("getAllDeposits exception caught", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Get the balance of a given user.
     *
     * @param name The name of the user to check.
     * @return The balance or an INTERNAL_SERVER_ERROR status.
     */
    @GetMapping("/balance/user/{name}")
    public ResponseEntity<Double> getUserBalance(@PathVariable String name) {
        getLogger().trace("getUserBalance called with parameter {}", name);

        User result = null;
        try {
            result = userService.getByName(name);
        } catch (Exception e) {
            getLogger().error("getUserBalance exception caught", e);
        }

        if (null != result) {
            double balance = depositService.getUserBalance(result);
            getLogger().trace("getUserBalance finished");
            return new ResponseEntity<>(balance, HttpStatus.OK);
        } else {
            getLogger().warn("getUserBalance failed with parameter {}", name);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get the list of all the deposits of a company.
     *
     * @param name The name of the company.
     * @return A list of {@link Deposit} object.
     */
    @GetMapping("/company/{name}")
    public ResponseEntity<List<Deposit>> getAllDepositsOfCompany(@PathVariable String name) {
        getLogger().trace("getAllDepositsOfCompany called");

        List<Deposit> result;
        try {
            result = depositService.getAllDepositsOfCompany(name);
        } catch (Exception e) {
            getLogger().error("getAllDepositsOfCompany exception caught", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Get the list of all the deposits of a user.
     *
     * @param name The name of the company.
     * @return A list of {@link Deposit} object.
     */
    @GetMapping("/user/{name}")
    public ResponseEntity<List<Deposit>> getAllDepositsOfUser(@PathVariable String name) {
        getLogger().trace("getAllDepositsOfUser called");

        List<Deposit> result;
        try {
            result = depositService.getAllDepositsOfUser(name);
        } catch (Exception e) {
            getLogger().error("getAllDepositsOfUser exception caught", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Add a meal deposit to a specific company/user combo. They must exist.
     *
     * @param companyName The name of the company.
     * @param username    The name of the user.
     * @param amount      the amount of the deposit.
     * @return HTTP status code OK if the operation was successful, else INTERNAL_SERVER_ERROR.
     */
    @PostMapping("/meal/{companyName}/{username}/{amount}")
    public ResponseEntity<Void> addMealDeposit(
            @PathVariable String companyName,
            @PathVariable String username,
            @PathVariable double amount) {
        getLogger().trace("addMealDeposit called with parameter {}/{}/{}", companyName, username, amount);

        boolean result = false;
        try {
            result = depositService.createMealDeposit(companyName, username, amount);
        } catch (Exception e) {
            getLogger().error("addMealDeposit exception caught", e);
        }

        if (result) {
            getLogger().trace("addMealDeposit finished");
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            getLogger().warn("addMealDeposit failed to add deposit {}/{}/{}", companyName, username, amount);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Add a gift deposit to a specific company/user combo. They must exist.
     *
     * @param companyName The name of the company.
     * @param username    The name of the user.
     * @param amount      the amount of the deposit.
     * @return HTTP status code OK if the operation was successful, else INTERNAL_SERVER_ERROR.
     */
    @PostMapping("/gift/{companyName}/{username}/{amount}")
    public ResponseEntity<Void> addGiftDeposit(
            @PathVariable String companyName,
            @PathVariable String username,
            @PathVariable double amount) {
        getLogger().trace("addGiftDeposit called with parameter {}/{}/{}", companyName, username, amount);

        boolean result = false;
        try {
            result = depositService.createGiftDeposit(companyName, username, amount);
        } catch (Exception e) {
            getLogger().error("addGiftDeposit exception caught", e);
        }

        if (result) {
            getLogger().trace("addGiftDeposit finished");
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            getLogger().warn("addGiftDeposit failed to add deposit {}/{}/{}", companyName, username, amount);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Delete a deposit given its id.
     *
     * @param id the id of the deposit to delete.
     * @return HTTP status OK if deleted, else INTERNAL_SERVER_ERROR.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteDeposit(@PathVariable String id) {
        getLogger().trace("deleteDeposit called with parameter {}", id);
        Deposit deposit;
        try {
            depositService.delete(id);
            deposit = depositService.getById(id);
        } catch (Exception e) {
            getLogger().error("deleteDeposit exception caught", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (null == deposit) {
            getLogger().trace("deleteDeposit finished");
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            getLogger().warn("deleteDeposit failed to delete deposit {}", id);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
