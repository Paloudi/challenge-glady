package com.challenge.glady.Controller;

import com.challenge.glady.Core.Entity.User;
import com.challenge.glady.Core.Interface.IEntity;
import com.challenge.glady.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * The UserController is a RESTful controller responsible for managing operations related to User entities.
 */
@RestController
@RequestMapping("/api/user")
public class UserController implements IEntity {

    /**
     * The {@link UserService} service.
     */
    private final UserService userService;

    /**
     * Create a new UserController.
     *
     * @param userService The service used by this controller.
     */
    @Autowired
    public UserController(UserService userService) {
        getLogger().info("User controller created");
        this.userService = userService;
    }

    /**
     * Get the list of all the users.
     *
     * @return A list of {@link User} object.
     */
    @GetMapping
    public ResponseEntity<List<User>> getAllUsers() {
        getLogger().trace("GetAllUsers called");
        return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
    }

    /**
     * Get a user from a name.
     *
     * @param name The name to use.
     * @return The user if found, else null.
     */
    @GetMapping("/{name}")
    public ResponseEntity<User> getUser(@PathVariable String name) {
        getLogger().trace("getUser called with parameter {}", name);
        User result = userService.getByName(name);
        if (null != result) {
            getLogger().trace("getUser finished");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
            getLogger().warn("getUser failed with parameter {}", name);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Add a user.
     *
     * @param user The user to insert.
     * @return HTTP status code OK if the operation was successful, else INTERNAL_SERVER_ERROR.
     */
    @PostMapping
    public ResponseEntity<Void> addUser(@RequestBody User user) {
        getLogger().trace("addUser called with parameter {}", user.getUsername());
        User result = userService.create(user);
        if (null != result) {
            getLogger().trace("addUser finished");
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            getLogger().warn("addUser failed to add user {}", user.getUsername());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update a user.
     *
     * @param user The user to update.
     * @return HTTP status code OK if the operation was successful, else INTERNAL_SERVER_ERROR.
     */
    @PutMapping
    public ResponseEntity<Void> updateUser(@RequestBody User user) {
        getLogger().trace("updateUser called with parameter {}", user.getUsername());

        User result = userService.update(user);
        if (null != result) {
            getLogger().trace("updateUser finished");
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            getLogger().warn("updateUser failed to update user {}", user.getUsername());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Delete a user.
     *
     * @param name The name of the user to delete.
     * @return HTTP status code OK if the operation was successful, else INTERNAL_SERVER_ERROR.
     */
    @DeleteMapping("/{name}")
    public ResponseEntity<Void> deleteUser(@PathVariable String name) {
        getLogger().trace("deleteUser called with parameter {}", name);
        boolean result = userService.delete(name);
        if (result) {
            getLogger().trace("deleteUser finished");
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            getLogger().warn("deleteUser failed with user {}", name);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
