package com.challenge.glady.Controller;

import com.challenge.glady.Core.Entity.Company;
import com.challenge.glady.Core.Interface.IEntity;
import com.challenge.glady.Service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * The CompanyController is a RESTful controller responsible for managing operations related to Company entities.
 */
@RestController
@RequestMapping("/api/company")
public class CompanyController implements IEntity {

    /**
     * The {@link CompanyService} service.
     */
    private final CompanyService companyService;

    /**
     * Create a new CompanyController.
     *
     * @param companyService The service used by this controller.
     */
    @Autowired
    public CompanyController(CompanyService companyService) {
        getLogger().info("Company Controller created");
        this.companyService = companyService;
    }

    /**
     * Get the list of all the companies.
     *
     * @return A list of {@link Company} object.
     */
    @GetMapping
    public ResponseEntity<List<Company>> getAllCompanies() {
        getLogger().trace("GetAllCompanies called");
        return new ResponseEntity<>(companyService.getAllCompanies(), HttpStatus.OK);
    }

    /**
     * Get the balance of a given company.
     *
     * @param name The name of the company to check.
     * @return The balance or an INTERNAL_SERVER_ERROR status.
     */
    @GetMapping("/balance/{name}")
    public ResponseEntity<Double> getCompanyBalance(@PathVariable String name) {
        getLogger().trace("getCompanyBalance called with parameter {}", name);
        Company result = companyService.getByName(name);
        if (null != result) {
            getLogger().trace("getCompanyBalance finished");
            return new ResponseEntity<>(result.getBalance(), HttpStatus.OK);
        } else {
            getLogger().warn("getCompanyBalance failed with parameter {}", name);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get a company by its name.
     *
     * @param name The name of the company we're looking for.
     * @return The company or an INTERNAL_SERVER_ERROR status.
     */
    @GetMapping("/{name}")
    public ResponseEntity<Company> getCompanyByName(@PathVariable String name) {
        getLogger().trace("getCompanyByName called with parameter {}", name);
        Company result = companyService.getByName(name);
        if (null != result) {
            getLogger().trace("GetCompanyByName finished");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
            getLogger().warn("GetCompanyByName failed with parameter {}", name);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Add a company.
     *
     * @param company The company to insert.
     * @return HTTP status code OK if the operation was successful, else INTERNAL_SERVER_ERROR.
     */
    @PostMapping
    public ResponseEntity<Void> addCompany(@RequestBody Company company) {
        getLogger().trace("addCompany called with parameter {}", company.getName());
        Company result = companyService.create(company);
        if (null != result) {
            getLogger().trace("addCompany finished");
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            getLogger().warn("addCompany failed to add company {}", company.getName());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Add balance to a company.
     *
     * @param name   The company to modify.
     * @param amount The amount to add, can be negative.
     * @return HTTP status code OK if the operation was successful, else INTERNAL_SERVER_ERROR.
     */
    @PatchMapping("/{name}/{amount}")
    public ResponseEntity<Void> addCompanyBalance(@PathVariable String name, @PathVariable double amount) {
        getLogger().trace("addCompanyBalance called with parameter {}/{}", name, amount);
        Company company = companyService.getByName(name);
        if (null != company) {
            // no check on negative numbers, could be possible and no precision on it, IDK.
            company.setBalance(company.getBalance() + amount);
            companyService.update(company);
            getLogger().trace("addCompanyBalance finished");
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            getLogger().warn("Tried to add balance to a non existing company {}", name);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update a company.
     *
     * @param company The company to update.
     * @return HTTP status code OK if the operation was successful, else INTERNAL_SERVER_ERROR.
     */
    @PutMapping
    public ResponseEntity<Void> updateCompany(@RequestBody Company company) {
        getLogger().trace("updateCompany called with parameter {}", company.getName());
        Company result = companyService.update(company);
        if (null != result) {
            getLogger().trace("updateCompany finished");
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            getLogger().warn("updateCompany failed to update company {}", company.getName());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Delete a company.
     *
     * @param name The name of the company to delete.
     * @return HTTP status code OK if the operation was successful, else INTERNAL_SERVER_ERROR.
     */
    @DeleteMapping("/{name}")
    public ResponseEntity<Void> deleteCompany(@PathVariable String name) {
        getLogger().trace("deleteCompany called with parameter {}", name);
        boolean result = companyService.delete(name);
        if (result) {
            getLogger().trace("deleteCompany finished");
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            getLogger().warn("deleteCompany failed with company {}", name);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
