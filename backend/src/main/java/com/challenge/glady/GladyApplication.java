package com.challenge.glady;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The application starting class.
 */
@SpringBootApplication
public class GladyApplication {

    /**
     * Method ran when the application is launched.
     *
     * @param args Command line arguments. We don't use them but Spring is.
     */
    public static void main(String[] args) {
        SpringApplication.run(GladyApplication.class, args);
    }

}
