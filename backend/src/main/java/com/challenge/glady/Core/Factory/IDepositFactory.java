package com.challenge.glady.Core.Factory;

import com.challenge.glady.Core.Entity.Company;
import com.challenge.glady.Core.Entity.Deposit;
import com.challenge.glady.Core.Entity.User;

import java.time.LocalDate;

/**
 * Deposit factory to handle the creation of deposits.
 */
public interface IDepositFactory {

    /**
     * Create a new Deposit object. Method to be implemented by each deposit type.
     *
     * @param amount  The amount of the deposit.
     * @param date    The date of the deposit.
     * @param company The company which deposed the money.
     * @param user    The user who received the money.
     * @return A {@link Deposit} object.
     */
    Deposit createDeposit(double amount, LocalDate date, Company company, User user);
}

