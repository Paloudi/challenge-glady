package com.challenge.glady.Core.Entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a user who can receive deposits (gift or meal) from companies.
 * Each user has a unique username which can be used for various operations.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User {

    /**
     * The unique identifier for the user.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The username for the user. This should be unique across the system.
     */
    @Column(unique = true)
    private String username;

    /**
     * The deposits linked to this user.
     */
    @JsonManagedReference(value = "user-deposit")
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Deposit> deposits = new ArrayList<>();

    /**
     * Creates a new user with the specified username.
     *
     * @param username The username for the user.
     */
    public User(String username) {
        this.username = username;
    }
}