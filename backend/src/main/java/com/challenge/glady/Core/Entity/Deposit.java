package com.challenge.glady.Core.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

/**
 * Represents a deposit, which can be of type gift or meal.
 * Each deposit has an associated amount, a distribution date, and a relationship
 * with the company that distributed it.
 * This is an abstract entity, intended to be extended by specific deposit types
 * like {@link GiftDeposit} or {@link MealDeposit}.
 **/
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "deposit_type", discriminatorType = DiscriminatorType.INTEGER)
@Table(
        name = "deposit",
        uniqueConstraints = @UniqueConstraint(
                columnNames = {"company_id", "user_id", "distributionDate", "deposit_type"})
)
@Getter
@Setter
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = MealDeposit.class, name = "meal"),
        @JsonSubTypes.Type(value = GiftDeposit.class, name = "gift")
})
public abstract class Deposit {

    /**
     * The unique identifier for the deposit.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The monetary amount of the deposit.
     */
    private double amount;

    /**
     * The date on which this deposit was distributed.
     */
    private LocalDate distributionDate;

    /**
     * The company that distributed this deposit.
     */
    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id")
    private Company company;

    /**
     * The user that received this deposit.
     */
    @JsonBackReference(value = "user-deposit")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    /**
     * Returns true if the deposit is expired.
     *
     * @return True is expired, else false.
     */
    public abstract boolean isExpired();
}
