package com.challenge.glady.Core.Factory;

import com.challenge.glady.Core.Entity.Company;
import com.challenge.glady.Core.Entity.Deposit;
import com.challenge.glady.Core.Entity.GiftDeposit;
import com.challenge.glady.Core.Entity.User;

import java.time.LocalDate;

/**
 * Factory implementation for the {@link GiftDeposit} object.
 */
public class GiftDepositFactory implements IDepositFactory {

    /**
     * Create a new GiftDeposit object.
     *
     * @param amount  The amount of the deposit.
     * @param date    The date of the deposit.
     * @param company The company which deposed the money.
     * @param user    The user who received the money.
     * @return A {@link Deposit} object.
     */
    @Override
    public Deposit createDeposit(double amount, LocalDate date, Company company, User user) {
        return new GiftDeposit(amount, date, company, user);
    }
}
