package com.challenge.glady.Core.Strategy;

import com.challenge.glady.Core.Interface.IExpiryStrategy;

import java.time.LocalDate;

/**
 * Strategy for determining the expiration of a meal deposit.
 * A meal deposit expires at the end of February of the year following its distribution date.
 */
public class MealExpiryStrategy implements IExpiryStrategy {

    /**
     * Determines if a meal deposit has expired based on its distribution date.
     *
     * @param distributionDate The date when the deposit was distributed.
     * @return true if the deposit has expired, false otherwise.
     */
    @Override
    public boolean isExpired(LocalDate distributionDate) {
        int yearOfExpiration = distributionDate.getYear() + 1;
        if ((yearOfExpiration % 4 == 0) && (yearOfExpiration % 100 != 0 || yearOfExpiration % 400 == 0)) {
            return LocalDate.now().isAfter(LocalDate.of(yearOfExpiration, 2, 29));
        } else {
            return LocalDate.now().isAfter(LocalDate.of(yearOfExpiration, 2, 28));
        }
    }
}
