package com.challenge.glady.Core.Interface;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base service for common dependencies.
 */
public interface IEntity {

    /**
     * Returns the logger for the implementing class.
     *
     * @return The logger.
     */
    default Logger getLogger() {
        return LoggerFactory.getLogger(this.getClass());
    }
}
