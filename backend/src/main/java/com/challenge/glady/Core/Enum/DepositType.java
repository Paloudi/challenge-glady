package com.challenge.glady.Core.Enum;

/**
 * Specify the type of deposit needed when using the factory.
 */
public enum DepositType {
    /**
     * Represent a {@link com.challenge.glady.Core.Entity.GiftDeposit} object.
     */
    GIFT,

    /**
     * Represent a {@link com.challenge.glady.Core.Entity.MealDeposit} object.
     */
    MEAL
}
