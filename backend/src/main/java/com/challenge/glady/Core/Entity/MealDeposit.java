package com.challenge.glady.Core.Entity;

import com.challenge.glady.Core.Strategy.MealExpiryStrategy;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

/**
 * Represents a meal deposit distributed to users by companies.
 * Meal deposits expire at the end of February of the year following
 * the distribution date.
 * For example, if a user receives a meal deposit on 01/01/2023,
 * it will expire on 28/02/2024 (or 29/02/2024 if it's a leap year).
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@DiscriminatorValue("1")
public class MealDeposit extends Deposit {

    /**
     * Creates a new MealDeposit and link to it a company and a user.
     *
     * @param amount           The amount of to distribute.
     * @param distributionDate The date of the distribution.
     * @param company          The distributing company.
     * @param user             The receiving user.
     */
    public MealDeposit(double amount, LocalDate distributionDate, Company company, User user) {
        // I'm letting negative amount possible, I don't have more information on it,
        // a user might owe money to the company for whatever reason.
        this.setAmount(amount);
        this.setDistributionDate(distributionDate);
        this.setCompany(company);
        this.setUser(user);
    }

    /**
     * Computes the expiration date of the meal deposit.
     * The expiration date is set to the end of February of the year
     * following the distribution date.
     *
     * @return the expiration date of the meal deposit.
     */
    public LocalDate computeExpirationDate() {
        // Possible optimization here, we could precompute the value then store it, trading CPU power for RAM or disk
        // usage.
        int yearOfExpiration = getDistributionDate().getYear() + 1;
        if (isLeapYear(yearOfExpiration)) {
            return LocalDate.of(yearOfExpiration, 2, 29);
        } else {
            return LocalDate.of(yearOfExpiration, 2, 28);
        }
    }

    /**
     * Checks if the gift deposit has expired based on the current strategy.
     *
     * @return true if the deposit has expired, false otherwise.
     */
    @Override
    public boolean isExpired() {
        MealExpiryStrategy expiryStrategy = new MealExpiryStrategy();
        return expiryStrategy.isExpired(getDistributionDate());
    }

    /**
     * Checks if a given year is a leap year.
     * Could be put in a Util class.
     *
     * @param year The year to check.
     * @return true if the given year is a leap year, false otherwise.
     */
    private boolean isLeapYear(int year) {
        return (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0);
    }
}
