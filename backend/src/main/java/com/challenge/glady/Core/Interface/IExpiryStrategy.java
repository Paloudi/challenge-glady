package com.challenge.glady.Core.Interface;

import java.time.LocalDate;

/**
 * Defines the strategy for checking the expiration of a deposit.
 */
public interface IExpiryStrategy {

    /**
     * Determines if a deposit has expired based on its distribution date.
     *
     * @param distributionDate The date when the deposit was distributed.
     * @return true if the deposit has expired, false otherwise.
     */
    boolean isExpired(LocalDate distributionDate);
}
