package com.challenge.glady.Core.Strategy;

import com.challenge.glady.Core.Interface.IExpiryStrategy;

import java.time.LocalDate;

/**
 * Strategy for determining the expiration of a gift deposit.
 * A gift deposit expire one year after its distribution date.
 */
public class GiftExpiryStrategy implements IExpiryStrategy {

    /**
     * Determines if a gift deposit has expired based on its distribution date.
     * A gift deposit expires 365 days after its distribution date.
     *
     * @param distributionDate The date when the deposit was distributed.
     * @return true if the deposit has expired, false otherwise.
     */
    @Override
    public boolean isExpired(LocalDate distributionDate) {
        return LocalDate.now().isAfter(distributionDate.plusDays(365));
    }
}
