package com.challenge.glady.Core.Entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a company that can distribute gift and meal deposits.
 * Companies have a balance which is checked before they can distribute any deposits.
 * Each company can have multiple deposits associated with it (OneToMany relationship).
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Company {

    /**
     * The unique identifier for the company.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The name of the company.
     */
    @Column(unique = true)
    private String name;

    /**
     * The available balance of the company. This balance is checked before distributing any deposits.
     */
    private double balance; // Assuming balance is stored as a double.

    /**
     * The deposits distributed by this company.
     */
    @JsonManagedReference
    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Deposit> deposits = new ArrayList<>();

    /**
     * Creates a new company with the specified name and balance.
     *
     * @param name    The name of the company.
     * @param balance The initial balance of the company.
     */
    public Company(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }
}
