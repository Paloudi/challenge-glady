package com.challenge.glady.Core.Entity;

import com.challenge.glady.Core.Strategy.GiftExpiryStrategy;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

/**
 * Represents a gift deposit distributed to users by companies.
 * Gift deposits have a lifespan of 365 days. Beyond this period,
 * they will no longer be counted in the user's balance.
 * For example, if a user receives a gift deposit on 01/01/2023,
 * it will expire on 12/31/2023. (Not usable AFTER this date, so the 01/01/2024)
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@DiscriminatorValue("0")
public class GiftDeposit extends Deposit {

    /**
     * Creates a new GiftDeposit and link to it a company and a user.
     *
     * @param amount           The amount of to distribute.
     * @param distributionDate The date of the distribution.
     * @param company          The distributing company.
     * @param user             The receiving user.
     */
    public GiftDeposit(double amount, LocalDate distributionDate, Company company, User user) {
        // I'm letting negative amount possible, I don't have more information on it,
        // a user might owe money to the company for whatever reason.
        this.setAmount(amount);
        this.setDistributionDate(distributionDate);
        this.setCompany(company);
        this.setUser(user);
    }

    /**
     * Computes the expiration date of the gift deposit.
     * The expiration date is calculated based on the distribution date
     * and the defined lifespan of a gift deposit, which is 365 days.
     *
     * @return the expiration date of the gift deposit.
     */
    public LocalDate computeExpirationDate() {
        // Possible optimization here, we could precompute the value then store it, trading CPU power for RAM or disk
        // usage.
        return getDistributionDate().plusDays(365);
    }

    /**
     * Checks if the gift deposit has expired based on the current strategy.
     *
     * @return true if the deposit has expired, false otherwise.
     */
    @Override
    public boolean isExpired() {
        GiftExpiryStrategy expiryStrategy = new GiftExpiryStrategy();
        return expiryStrategy.isExpired(getDistributionDate());
    }
}
