package com.challenge.glady;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test the Spring boot application itself.
 */
@SpringBootTest
class GladyApplicationTests {

	/**
	 * The application context.
	 */
	@Autowired
	private ApplicationContext ctx;

	/**
	 * Run the Spring boot starting logic to see if the application is able to run.
	 */
	@Test
	public void contextLoads() {
		assertThat(ctx).isNotNull();
	}

}
