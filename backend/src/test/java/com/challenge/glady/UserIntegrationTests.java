package com.challenge.glady;

import com.challenge.glady.Core.Entity.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Integration tests for User-related functionalities.
 * <p>
 * This test class aims to test the behavior of the User-related
 * endpoints by making actual HTTP requests and verifying the response.
 * It leverages the Spring Boot Test framework to start an embedded server on a random port.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class UserIntegrationTests {

    /**
     * Endpoint path for company-related actions.
     */
    private static String endpoint;

    /**
     * Utility to perform HTTP operations in tests.
     * It is a simplistic client HTTP client tailored for testing.
     * Injected by Spring Boot.
     */
    private final RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());

    /**
     * Port used by the test.
     */
    @LocalServerPort
    private int port;

    @BeforeEach
    public void setup() {
        endpoint = "http://localhost:" + port + "/api/user";
    }

    /**
     * Test method to create, read, update, then delete (CRUD) a user.
     * <p>
     */
    @Test
    public void userTypicalUsageTest() {
        String username = "Michel";
        // CREATE
        {
            // Arrange
            User newUser = new User();
            newUser.setUsername(username);

            // Act
            ResponseEntity<Void> response = restTemplate.postForEntity(endpoint, newUser, Void.class);

            // Assert
            assertEquals(HttpStatus.OK, response.getStatusCode());
        }

        // READ
        {
            // Act
            ResponseEntity<User> response = restTemplate.getForEntity(endpoint + "/" + username, User.class);

            // Assert
            assertEquals(HttpStatus.OK, response.getStatusCode());
            assertNotNull(response.getBody());
            assertEquals(username, response.getBody().getUsername());
        }

        // UPDATE
        {
            // Arrange
            User updatedUser = new User();
            updatedUser.setUsername(username);
            List<Deposit> depositList = new ArrayList<>();
            depositList.add(new GiftDeposit(100.0, LocalDate.now(), new Company(), updatedUser));
            depositList.add(new MealDeposit(100.0, LocalDate.now(), new Company(), updatedUser));
            updatedUser.setDeposits(depositList);

            // Act
            restTemplate.put(endpoint, updatedUser);

            // Assert (retrieve the user to check if it was updated)
            ResponseEntity<User>
                    response =
                    restTemplate.getForEntity(endpoint + "/" + updatedUser.getUsername(), User.class);
            assertNotNull(response.getBody());
            assertEquals(2, response.getBody().getDeposits().size()); // Don't compare the entities themselves, they're not the same
        }

        // DELETE
        {
            // Act
            restTemplate.delete(endpoint + "/" + username);

            // Assert (try to retrieve the deleted user, expecting an INTERNAL_SERVER_ERROR status)
            try {
                restTemplate.getForEntity(endpoint + "/" + username, User.class);
                fail("Expected an HttpServerErrorException to be thrown");
            } catch (HttpServerErrorException e) {
                assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatusCode());
            }
        }
    }
}
