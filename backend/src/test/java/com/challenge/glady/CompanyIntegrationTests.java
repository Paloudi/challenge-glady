package com.challenge.glady;

import com.challenge.glady.Core.Entity.Company;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Integration tests for Company-related functionalities.
 * <p>
 * This test class aims to test the behavior of the Company-related
 * endpoints by making actual HTTP requests and verifying the response.
 * It leverages the Spring Boot Test framework to start an embedded server on a random port.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class CompanyIntegrationTests {

    /**
     * Endpoint path for company-related actions.
     */
    private static String endpoint;

    /**
     * Utility to perform HTTP operations in tests.
     * It is a simplistic client HTTP client tailored for testing.
     * Injected by Spring Boot.
     */
    private final RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());

    /**
     * Port used by the test.
     */
    @LocalServerPort
    private int port;

    /**
     * Set the endpoint, could not do it as a field or in a BeforeAll method
     * because the port value is not initialized at this moment.
     */
    @BeforeEach
    public void setup() {
        endpoint = "http://localhost:" + port + "/api/company";
    }

    /**
     * Test method to create, read, update, then delete (CRUD) a company.
     */
    @Test
    public void companyTypicalUsageTest() {
        String companyName = "TestCo";
        // CREATE
        {
            // Arrange
            Company newCompany = new Company();
            newCompany.setName(companyName);
            newCompany.setBalance(1000.0);

            // Act
            ResponseEntity<Void> response = restTemplate.postForEntity(endpoint, newCompany, Void.class);

            // Assert
            assertEquals(HttpStatus.OK, response.getStatusCode());
        }

        // READ
        {
            // Act
            ResponseEntity<Company> response = restTemplate.getForEntity(endpoint + "/" + companyName, Company.class);

            // Assert
            assertEquals(HttpStatus.OK, response.getStatusCode());
            assertNotNull(response.getBody());
            assertEquals(companyName, response.getBody().getName());
        }

        // UPDATE
        {
            // Arrange
            Company updatedCompany = new Company();
            updatedCompany.setName("TestCo");
            updatedCompany.setBalance(2000.0);

            // Act
            restTemplate.put(endpoint, updatedCompany);

            // Assert (retrieve the company to check if it was updated)
            ResponseEntity<Company>
                    response =
                    restTemplate.getForEntity(endpoint + "/" + updatedCompany.getName(), Company.class);
            assertNotNull(response.getBody());
            assertEquals(2000.0, response.getBody().getBalance(), 0.001);
        }

        // DELETE
        {
            // Act
            restTemplate.delete(endpoint + "/" + companyName);

            // Assert (try to retrieve the deleted company, expecting an INTERNAL_SERVER_ERROR status)
            try {
                restTemplate.getForEntity(endpoint + "/" + companyName, Company.class);
                fail("Expected an HttpServerErrorException to be thrown");
            } catch (HttpServerErrorException e) {
                assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatusCode());
            }
        }
    }

    /**
     * Test method to create, add balance, check the balance, then delete a company.
     */
    @Test
    public void companyBalanceTest() {
        String companyName = "BalanceTestCo";
        // CREATE
        {
            // Arrange
            Company newCompany = new Company();
            newCompany.setName(companyName);
            newCompany.setBalance(1000.0);

            // Act
            ResponseEntity<Void> response = restTemplate.postForEntity(endpoint, newCompany, Void.class);

            // Assert
            assertEquals(HttpStatus.OK, response.getStatusCode());
        }

        // ADD BALANCE
        {
            // Arrange
            double amountToAdd = 500.0;

            // Act
            restTemplate.patchForObject(endpoint + "/" + companyName + "/" + amountToAdd, null, Void.class);

            // Assert (retrieve the company to check the updated balance)
            ResponseEntity<Company> response = restTemplate.getForEntity(endpoint + "/" + companyName, Company.class);
            assertNotNull(response.getBody());
            assertEquals(1500.0, response.getBody().getBalance(), 0.001);  // Expecting 1500 since we started with
            // 1000 and added 500
        }

        // CHECK BALANCE
        {
            // Act
            ResponseEntity<Double>
                    response =
                    restTemplate.getForEntity(endpoint + "/balance/" + companyName, Double.class);

            // Assert
            assertEquals(HttpStatus.OK, response.getStatusCode());
            assertNotNull(response.getBody());
            assertEquals(1500.0, response.getBody(), 0.001);  // Still expecting 1500
        }

        // DELETE
        {
            // Act
            restTemplate.delete(endpoint + "/" + companyName);

            // Assert (try to retrieve the deleted company, expecting an INTERNAL_SERVER_ERROR status)
            try {
                restTemplate.getForEntity(endpoint + "/" + companyName, Company.class);
                fail("Expected an HttpServerErrorException to be thrown");
            } catch (HttpServerErrorException e) {
                assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatusCode());
            }
        }
    }
}
