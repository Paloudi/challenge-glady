package com.challenge.glady.Controller;

import com.challenge.glady.Core.Entity.*;
import com.challenge.glady.Service.DepositService;
import com.challenge.glady.Service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test the {@link DepositController} behavior.
 * Services are mocked.
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(DepositController.class)
public class DepositControllerTests {

    /**
     * The MockMVC.
     */
    @Autowired
    private MockMvc mockMvc;

    /**
     * The ObjectMapper.
     */
    @Autowired
    private ObjectMapper objectMapper;

    /**
     * The {@link UserService} service.
     */
    @MockBean
    private UserService userService;

    /**
     * The {@link DepositService} service.
     */
    @MockBean
    private DepositService depositService;

    /**
     * The controller should return an OK when getting an existing deposit.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void getDepositByIdTestExistingDeposit() throws Exception {
        // Arrange
        String id = "1";
        Deposit expectedDeposit = new GiftDeposit(500, LocalDate.now(), new Company(), new User());
        when(depositService.getById(anyString())).thenReturn(expectedDeposit);

        // Act & Assert
        mockMvc.perform(get("/api/deposit/" + id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedDeposit)));
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when getting a not existing deposit.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void getDepositByIdTestNotExistingDeposit() throws Exception {
        // Arrange
        String id = "1";
        when(depositService.getById(anyString())).thenReturn(null);

        // Act & Assert
        mockMvc.perform(get("/api/deposit/" + id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when getting an exception.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void getDepositByIdTestException() throws Exception {
        // Arrange
        String id = "1";
        when(depositService.getById(anyString())).thenThrow(RuntimeException.class);

        // Act & Assert
        mockMvc.perform(get("/api/deposit/" + id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an OK when getting all deposits.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void getAllDepositsSuccess() throws Exception {
        // Arrange
        List<Deposit> depositList = new ArrayList<>();
        depositList.add(new MealDeposit(0.0, LocalDate.now(), new Company(), new User()));
        depositList.add(new GiftDeposit(0.0, LocalDate.now(), new Company(), new User()));
        when(depositService.getAllDeposits()).thenReturn(depositList);

        // Act & Assert
        mockMvc.perform(get("/api/deposit")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(depositList)));
    }

    /**
     * The controller should return an INTERNAL_SERVER_EXCEPTION when failing to get all deposits.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void getAllDepositsException() throws Exception {
        // Arrange
        when(depositService.getAllDeposits()).thenThrow(RuntimeException.class);

        // Act & Assert
        mockMvc.perform(get("/api/deposit")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an OK when getting a user balance.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void getUserBalanceTestExistingUser() throws Exception {
        // Arrange
        String userName = "user1";
        User user = new User(userName);
        double expectedBalance = 100.0;
        when(userService.getByName(userName)).thenReturn(user);
        when(depositService.getUserBalance(user)).thenReturn(expectedBalance);

        // Act & Assert
        mockMvc.perform(get("/api/deposit/balance/user/" + userName))
                .andExpect(status().isOk())
                .andExpect(content().string(String.valueOf(expectedBalance)));
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when failing to get a user balance.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void getUserBalanceTestNonExistingUser() throws Exception {
        // Arrange
        String userName = "user1";
        when(userService.getByName(userName)).thenReturn(null);

        // Act & Assert
        mockMvc.perform(get("/api/deposit/balance/user/" + userName))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when an exception is thrown.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void getUserBalanceTestException() throws Exception {
        // Arrange
        String userName = "user1";
        when(userService.getByName(userName)).thenThrow(RuntimeException.class);

        // Act & Assert
        mockMvc.perform(get("/api/deposit/balance/user/" + userName))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an OK when succeeding.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void getAllDepositsOfCompanyTestSuccess() throws Exception {
        // Arrange
        when(depositService.getAllDepositsOfCompany(anyString())).thenReturn(new ArrayList<>());

        // Act & Assert
        mockMvc.perform(get("/api/deposit/company/name")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(new ArrayList<>())));

    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when failing.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void getAllDepositsOfCompanyTestException() throws Exception {
        // Arrange
        when(depositService.getAllDepositsOfCompany(anyString())).thenThrow(RuntimeException.class);

        // Act & Assert
        mockMvc.perform(get("/api/deposit/company/name")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an OK when succeeding.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void getAllDepositsOfUserTestSuccess() throws Exception {
        // Arrange
        when(depositService.getAllDepositsOfUser(anyString())).thenReturn(new ArrayList<>());

        // Act & Assert
        mockMvc.perform(get("/api/deposit/user/name")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(new ArrayList<>())));

    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when failing.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void getAllDepositsOfUserTestException() throws Exception {
        // Arrange
        when(depositService.getAllDepositsOfUser(anyString())).thenThrow(RuntimeException.class);

        // Act & Assert
        mockMvc.perform(get("/api/deposit/user/name")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an OK when succeeding.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void addMealDepositTestSuccess() throws Exception {
        // Arrange
        when(depositService.createMealDeposit(anyString(), anyString(), anyDouble())).thenReturn(true);

        // Act & Assert
        mockMvc.perform(post("/api/deposit/meal/a/b/0"))
                .andExpect(status().isOk());
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when failing.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void addMealDepositTestFailure() throws Exception {
        // Arrange
        when(depositService.createMealDeposit(anyString(), anyString(), anyDouble())).thenReturn(false);

        // Act & Assert
        mockMvc.perform(post("/api/deposit/meal/a/b/0"))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when throwing an exception.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void addMealDepositTestException() throws Exception {
        // Arrange
        when(depositService.createMealDeposit(anyString(), anyString(), anyDouble())).thenThrow(RuntimeException.class);

        // Act & Assert
        mockMvc.perform(post("/api/deposit/meal/a/b/0"))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an OK when succeeding.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void addGiftDepositTestSuccess() throws Exception {
        // Arrange
        when(depositService.createGiftDeposit(anyString(), anyString(), anyDouble())).thenReturn(true);

        // Act & Assert
        mockMvc.perform(post("/api/deposit/gift/a/b/0"))
                .andExpect(status().isOk());
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when failing.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void addGiftDepositTestFailure() throws Exception {
        // Arrange
        when(depositService.createGiftDeposit(anyString(), anyString(), anyDouble())).thenReturn(false);

        // Act & Assert
        mockMvc.perform(post("/api/deposit/gift/a/b/0"))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when throwing an exception.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void addGiftDepositTestException() throws Exception {
        // Arrange
        when(depositService.createGiftDeposit(anyString(), anyString(), anyDouble())).thenThrow(RuntimeException.class);

        // Act & Assert
        mockMvc.perform(post("/api/deposit/gift/a/b/0"))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an OK when succeeding.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void deleteDepositTestSuccess() throws Exception {
        // Arrange
        when(depositService.getById(anyString())).thenReturn(null);

        // Act & Assert
        mockMvc.perform(delete("/api/deposit/0"))
                .andExpect(status().isOk());
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when failing.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void deleteDepositTestFailure() throws Exception {
        // Arrange
        when(depositService.getById(anyString())).thenReturn(new GiftDeposit());

        // Act & Assert
        mockMvc.perform(delete("/api/deposit/0"))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when throwing an exception.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void deleteDepositTestException() throws Exception {
        // Arrange
        when(depositService.getById(anyString())).thenThrow(RuntimeException.class);

        // Act & Assert
        mockMvc.perform(delete("/api/deposit/0"))
                .andExpect(status().isInternalServerError());
    }
}
