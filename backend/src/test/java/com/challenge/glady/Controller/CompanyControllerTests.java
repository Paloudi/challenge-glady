package com.challenge.glady.Controller;

import com.challenge.glady.Core.Entity.Company;
import com.challenge.glady.Service.CompanyService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test the {@link CompanyController} behavior.
 * Services are mocked.
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(CompanyController.class)
public class CompanyControllerTests {

    /**
     * The MockMVC.
     */
    @Autowired
    private MockMvc mockMvc;

    /**
     * The {@link CompanyService} service.
     */
    @MockBean
    private CompanyService companyService;

    /**
     * The ObjectMapper.
     */
    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Set up the mock for each test.
     *
     * @param webApplicationContext Spring web application context.
     */
    @BeforeEach
    public void setup(WebApplicationContext webApplicationContext) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    /**
     * The controller should return a list of companies when getting all companies.
     *
     * @throws Exception Mock internal exception
     */
    @Test
    public void getAllCompaniesTest() throws Exception {
        // Arrange
        List<Company> expectedCompanies = List.of(new Company("A", 1.0), new Company("B", 2.0));
        when(companyService.getAllCompanies()).thenReturn(expectedCompanies);

        // Act & Assert
        mockMvc.perform(get("/api/company"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedCompanies)));
    }

    /**
     * The controller should return an OK when getting a company balance.
     *
     * @throws Exception Mock Internal exception.
     */
    @Test
    public void getCompanyBalanceTestExistingUser() throws Exception {
        // Arrange
        String name = "A";
        double expectedBalance = 100.0;
        Company company = new Company(name, expectedBalance);
        when(companyService.getByName(name)).thenReturn(company);

        // Act & Assert
        mockMvc.perform(get("/api/company/balance/" + name))
                .andExpect(status().isOk())
                .andExpect(content().string(String.valueOf(expectedBalance)));
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when failing to get a company balance.
     *
     * @throws Exception Mock Internal exception.
     */
    @Test
    public void getCompanyBalanceTestNonExistingUser() throws Exception {
        // Arrange
        String name = "A";
        when(companyService.getByName("A")).thenReturn(null);

        // Act & Assert
        mockMvc.perform(get("/api/company/balance/" + name))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an OK and the found company when getting an existing company.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void getCompanyTestExistingUser() throws Exception {
        // Arrange
        Company expectedCompany = new Company("A", 100.0);
        when(companyService.getByName(expectedCompany.getName())).thenReturn(expectedCompany);

        // Act & Assert
        mockMvc.perform(get("/api/company/" + expectedCompany.getName()))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedCompany)));
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when failing to get a company.
     *
     * @throws Exception Mock internal exception.
     */
    @Test
    public void getCompanyTestNonExistingUser() throws Exception {
        // Arrange
        when(companyService.getByName("A")).thenReturn(null);

        // Act & Assert
        mockMvc.perform(get("/api/company/A"))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an OK when adding a company
     *
     * @throws Exception Mock internal exception
     */
    @Test
    public void addCompanyTestSuccess() throws Exception {
        // Arrange
        Company companyToAdd = new Company("A", 100.0);
        when(companyService.create(any(Company.class))).thenReturn(companyToAdd);

        // Act & Assert
        mockMvc.perform(post("/api/company")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(companyToAdd)))
                .andExpect(status().isOk());
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when failing to add a company
     *
     * @throws Exception Mock internal exception
     */
    @Test
    public void addCompanyTestFailure() throws Exception {
        // Arrange
        Company companyToAdd = new Company("A", 100.0);
        when(companyService.create(any(Company.class))).thenReturn(null);

        // Act & Assert
        mockMvc.perform(post("/api/company")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(companyToAdd)))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when trying to access a not existing company balance
     *
     * @throws Exception Mock internal exception
     */
    @Test
    public void addCompanyBalanceTestNonExisting() throws Exception {
        // Act & Assert
        mockMvc.perform(patch("/api/company/failure/500"))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an OK when updating a company
     *
     * @throws Exception Mock internal exception
     */
    @Test
    public void updateCompanyTestSuccess() throws Exception {
        // Arrange
        Company companyToUpdate = new Company("A", 100.0);
        when(companyService.update(any(Company.class))).thenReturn(companyToUpdate);

        // Act & Assert
        mockMvc.perform(put("/api/company")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(companyToUpdate)))
                .andExpect(status().isOk());
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when failing to update a company
     *
     * @throws Exception Mock internal exception
     */
    @Test
    public void updateCompanyTestFailure() throws Exception {
        // Arrange
        Company companyToUpdate = new Company("A", 100.0);
        when(companyService.update(any(Company.class))).thenReturn(null);

        // Act & Assert
        mockMvc.perform(put("/api/company")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(companyToUpdate)))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an OK when deleting a company
     *
     * @throws Exception Mock internal exception
     */
    @Test
    public void deleteCompanyTestSuccess() throws Exception {
        // Arrange
        String name = "A";
        when(companyService.delete(name)).thenReturn(true);

        // Act & Assert
        mockMvc.perform(delete("/api/company/" + name))
                .andExpect(status().isOk());
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when failing to delete a company
     *
     * @throws Exception Mock internal exception
     */
    @Test
    public void deleteCompanyTestFailure() throws Exception {
        // Arrange
        String name = "A";
        when(companyService.delete(name)).thenReturn(false);

        // Act & Assert
        mockMvc.perform(delete("/api/company/" + name))
                .andExpect(status().isInternalServerError());
    }
}
