package com.challenge.glady.Controller;

import com.challenge.glady.Service.UserService;
import com.challenge.glady.Core.Entity.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test the {@link UserController} behavior.
 * Services are mocked.
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
public class UserControllerTests {

    /**
     * The MockMVC.
     */
    @Autowired
    private MockMvc mockMvc;

    /**
     * The UserService.
     */
    @MockBean
    private UserService userService;

    /**
     * The ObjectMapper.
     */
    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Set up the mock for each test.
     * @param webApplicationContext Spring web application context.
     */
    @BeforeEach
    public void setup(WebApplicationContext webApplicationContext) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    /**
     * The controller should return a list of users when getting all users.
     * @throws Exception Mock internal exception
     */
    @Test
    public void getAllUsersTest() throws Exception {
        // Arrange
        List<User> expectedUsers = List.of(new User("user1"), new User("user2"));
        when(userService.getAllUsers()).thenReturn(expectedUsers);

        // Act & Assert
        mockMvc.perform(get("/api/user"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedUsers)));
    }

    /**
     * The controller should return an OK and the found user when getting an existing user.
     * @throws Exception Mock internal exception.
     */
    @Test
    public void getUserTestExistingUser() throws Exception {
        // Arrange
        User expectedUser = new User("user1");
        when(userService.getByName(expectedUser.getUsername())).thenReturn(expectedUser);

        // Act & Assert
        mockMvc.perform(get("/api/user/" + expectedUser.getUsername()))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedUser)));
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when failing to get a user.
     * @throws Exception Mock internal exception.
     */
    @Test
    public void getUserTestNonExistingUser() throws Exception {
        // Arrange
        when(userService.getByName("user1")).thenReturn(null);

        // Act & Assert
        mockMvc.perform(get("/api/user/user1"))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an OK when adding a user
     * @throws Exception Mock internal exception
     */
    @Test
    public void addUserTestSuccess() throws Exception {
        // Arrange
        User userToAdd = new User("user1");
        when(userService.create(any(User.class))).thenReturn(userToAdd);

        // Act & Assert
        mockMvc.perform(post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(userToAdd)))
                .andExpect(status().isOk());
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when failing to add a user.
     * @throws Exception Mock internal exception
     */
    @Test
    public void addUserTestFailure() throws Exception {
        // Arrange
        User userToAdd = new User("user1");
        when(userService.create(any(User.class))).thenReturn(null);

        // Act & Assert
        mockMvc.perform(post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(userToAdd)))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an OK when updating a user.
     * @throws Exception Mock internal exception
     */
    @Test
    public void updateUserTestSuccess() throws Exception {
        // Arrange
        User userToUpdate = new User("user1");
        when(userService.update(any(User.class))).thenReturn(userToUpdate);

        // Act & Assert
        mockMvc.perform(put("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(userToUpdate)))
                .andExpect(status().isOk());
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when failing to update a user.
     * @throws Exception Mock internal exception
     */
    @Test
    public void updateUserTestFailure() throws Exception {
        // Arrange
        User userToUpdate = new User("user1");
        when(userService.update(any(User.class))).thenReturn(null);

        // Act & Assert
        mockMvc.perform(put("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(userToUpdate)))
                .andExpect(status().isInternalServerError());
    }

    /**
     * The controller should return an OK when deleting a user.
     * @throws Exception Mock internal exception
     */
    @Test
    public void deleteUserTestSuccess() throws Exception {
        // Arrange
        String userName = "user1";
        when(userService.delete(userName)).thenReturn(true);

        // Act & Assert
        mockMvc.perform(delete("/api/user/" + userName))
                .andExpect(status().isOk());
    }

    /**
     * The controller should return an INTERNAL_SERVER_ERROR when failing to delete a user.
     * @throws Exception Mock internal exception
     */
    @Test
    public void deleteUserTestFailure() throws Exception {
        // Arrange
        String userName = "user1";
        when(userService.delete(userName)).thenReturn(false);

        // Act & Assert
        mockMvc.perform(delete("/api/user/" + userName))
                .andExpect(status().isInternalServerError());
    }
}
