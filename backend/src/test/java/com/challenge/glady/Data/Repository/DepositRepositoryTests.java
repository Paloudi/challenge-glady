package com.challenge.glady.Data.Repository;

import com.challenge.glady.Core.Entity.Company;
import com.challenge.glady.Core.Entity.Deposit;
import com.challenge.glady.Core.Entity.GiftDeposit;
import com.challenge.glady.Core.Entity.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test the repository methods by directly communicating with the database.
 * The database is not mocked, be careful when running this test depending on your configuration.
 */
@DataJpaTest
public class DepositRepositoryTests {

    /**
     * Test amount.
     */
    private final double amount = 75.0;

    /**
     * Test date.
     */
    private final LocalDate date = LocalDate.now();

    /**
     * Represent the {@link DepositRepository} repository to test.
     */
    @Autowired
    private DepositRepository depositRepository;

    /**
     * Represent the {@link CompanyRepository} repository to test.
     */
    @Autowired
    private CompanyRepository companyRepository;

    /**
     * Represent the {@link UserRepository} repository to test.
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * Represent the test database manager.
     */
    @Autowired
    private TestEntityManager entityManager;

    /**
     * Test user.
     */
    private User user = new User();

    /**
     * Test company.
     */
    private Company company = new Company();

    /**
     * Called before each test. Clean the database.
     */
    @BeforeEach
    public void setup() {
        depositRepository.deleteAll();
    }

    /**
     * Called after each test. Clean the database.
     */
    @AfterEach
    public void tearDown() {
        depositRepository.deleteAll();
    }

    /**
     * Create a deposit in the database and retrieves it.
     */
    @Test
    public void CreateDepositTest() {
        // Arrange
        String username = "Michel";
        String companyName = "Glady";
        company = new Company(companyName, 500.0);
        user = new User(username);
        companyRepository.save(company);
        userRepository.save(user);
        Deposit deposit = new GiftDeposit(amount, date, company, user);

        // Act
        Deposit savedDeposit = depositRepository.save(deposit);
        entityManager.flush();  // Ensure any pending changes are pushed to the database
        Deposit found = depositRepository.findById(savedDeposit.getId()).orElse(null);

        // Assert
        assertThat(found).isNotNull();
        assertThat(found.getAmount()).isEqualTo(amount);
        assertThat(found.getDistributionDate()).isEqualTo(date);
        assertThat(found.getUser()).isEqualTo(user);
        assertThat(found.getUser().getUsername()).isEqualTo(username);
        assertThat(found.getCompany()).isEqualTo(company);
        assertThat(found.getCompany().getName()).isEqualTo(companyName);
    }

    /**
     * Update a deposit in the database.
     */
    @Test
    public void UpdateUserTest() {
        // Arrange
        companyRepository.save(company);
        userRepository.save(user);
        double newAmount = (amount) * 2;
        Deposit deposit = new GiftDeposit(amount, date, company, user);
        Deposit savedDeposit = depositRepository.save(deposit);
        entityManager.flush();  // Ensure any pending changes are pushed to the database

        // Act
        Deposit found = depositRepository.findById(savedDeposit.getId()).orElse(null);
        if (found != null) {
            found.setAmount(newAmount);
            depositRepository.save(found);
            entityManager.flush();  // Ensure any pending changes are pushed to the database
        }

        // Assert
        assertThat(found).isNotNull();
        assertThat(found.getAmount()).isEqualTo(newAmount);
        assertThat(found.getDistributionDate()).isEqualTo(date);
        assertThat(found.getUser()).isEqualTo(user);
        assertThat(found.getCompany()).isEqualTo(company);
    }

    /**
     * Delete a deposit in the database.
     */
    @Test
    public void DeleteUserTest() {
        // Arrange
        companyRepository.save(company);
        userRepository.save(user);
        Deposit deposit = new GiftDeposit(amount, date, company, user);
        depositRepository.save(deposit);
        entityManager.flush();  // Ensure any pending changes are pushed to the database

        // Act
        depositRepository.delete(deposit);
        Deposit found = depositRepository.findById(deposit.getId()).orElse(null);

        // Assert
        assertThat(found).isNull();
    }
}
