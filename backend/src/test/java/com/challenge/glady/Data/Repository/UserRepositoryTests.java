package com.challenge.glady.Data.Repository;

import com.challenge.glady.Core.Entity.User;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test the repository methods by directly communicating with the database.
 * The database is not mocked, be careful when running this test depending on your configuration.
 */
@DataJpaTest
public class UserRepositoryTests {

    /**
     * Represent the {@link UserRepository} repository to test.
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * Represent the test database manager.
     */
    @Autowired
    private TestEntityManager entityManager;

    /**
     * Called before each test. Clean the database.
     */
    @BeforeEach
    public void setup() {
        userRepository.deleteAll();
    }

    /**
     * Called after each test. Clean the database.
     */
    @AfterEach
    public void tearDown() {
        userRepository.deleteAll();
    }

    /**
     * Create a user in the database and retrieves it.
     */
    @Test
    public void CreateUserTest() {
        // Arrange
        String userName = "Michel";
        User user = new User(userName);

        // Act
        User savedUser = userRepository.save(user);
        entityManager.flush();  // Ensure any pending changes are pushed to the database
        User found = userRepository.findById(savedUser.getId()).orElse(null);

        // Assert
        assertThat(found).isNotNull();
        assertThat(found.getUsername()).isEqualTo(userName);
    }

    /**
     * Update a user in the database.
     */
    @Test
    public void UpdateUserTest() {
        // Arrange
        String userName = "Michel";
        String newUserName = "Jacques";
        User user = new User(userName);
        User savedUser = userRepository.save(user);
        entityManager.flush();  // Ensure any pending changes are pushed to the database

        // Act
        User found = userRepository.findById(savedUser.getId()).orElse(null);
        if (found != null) {
            found.setUsername(newUserName);
            userRepository.save(found);
            entityManager.flush();  // Ensure any pending changes are pushed to the database
        }

        // Assert
        assertThat(found).isNotNull();
        assertThat(found.getUsername()).isEqualTo(newUserName);
    }

    /**
     * Delete a user in the database.
     */
    @Test
    public void DeleteUserTest() {
        // Arrange
        String userName = "Michel";
        User user = new User(userName);
        userRepository.save(user);
        entityManager.flush();  // Ensure any pending changes are pushed to the database

        // Act
        userRepository.delete(user);
        User found = userRepository.findById(user.getId()).orElse(null);

        // Assert
        assertThat(found).isNull();
    }
}
