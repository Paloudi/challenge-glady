package com.challenge.glady.Data.Repository;

import com.challenge.glady.Core.Entity.Company;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test the repository methods by directly communicating with the database.
 * The database is not mocked, be careful when running this test depending on your configuration.
 */
@DataJpaTest
public class CompanyRepositoryTests {

    /**
     * Represent the {@link CompanyRepository} repository to test.
     */
    @Autowired
    private CompanyRepository companyRepository;

    /**
     * Represent the test database manager.
     */
    @Autowired
    private TestEntityManager entityManager;

    /**
     * Create a company in the database and retrieves it.
     */
    @Test
    public void CreateCompanyTest() {
        // Arrange
        String companyName = "TestCompany";
        double balance = 100.0;
        Company company = new Company(companyName, balance);

        // Act
        Company savedCompany = companyRepository.save(company);
        entityManager.flush();  // Ensure any pending changes are pushed to the database
        Company found = companyRepository.findById(savedCompany.getId()).orElse(null);

        // Assert
        assertThat(found).isNotNull();
        assertThat(found.getName()).isEqualTo(companyName);
        assertThat(found.getBalance()).isEqualTo(balance);
    }

    /**
     * Update a company in the database.
     */
    @Test
    public void UpdateCompanyTest() {
        // Arrange
        String companyName = "Wedoogift";
        String newCompanyName = "Glady";
        double balance = 100.0;
        Company company = new Company(companyName, balance);
        companyRepository.save(company);
        entityManager.flush();  // Ensure any pending changes are pushed to the database

        // Act
        Company found = companyRepository.findById(company.getId()).orElse(null);
        if (found != null) {
            found.setName(newCompanyName);
            companyRepository.save(found);
            entityManager.flush();  // Ensure any pending changes are pushed to the database
        }

        // Assert
        assertThat(found).isNotNull();
        assertThat(found.getName()).isEqualTo(newCompanyName);
    }

    /**
     * Delete a company in the database.
     */
    @Test
    public void DeleteCompanyTest() {
        // Arrange
        String companyName = "Glady";
        double balance = 100.0;
        Company company = new Company(companyName, balance);
        companyRepository.save(company);
        entityManager.flush();  // Ensure any pending changes are pushed to the database

        // Act
        companyRepository.delete(company);
        Company found = companyRepository.findById(company.getId()).orElse(null);

        // Assert
        assertThat(found).isNull();
    }
}
