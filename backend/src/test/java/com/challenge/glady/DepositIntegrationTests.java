package com.challenge.glady;

import com.challenge.glady.Core.Entity.Company;
import com.challenge.glady.Core.Entity.Deposit;
import com.challenge.glady.Core.Entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Integration tests for Deposit-related functionalities.
 * <p>
 * This test class aims to test the behavior of the Deposit-related
 * endpoints by making actual HTTP requests and verifying the response.
 * It leverages the Spring Boot Test framework to start an embedded server on a random port.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class DepositIntegrationTests {

    /**
     * Endpoint path for deposit-related actions.
     */
    private static String depositEndpoint;

    /**
     * Endpoint path for company-related actions.
     */
    private static String companyEndpoint;

    /**
     * Endpoint path for user-related actions.
     */
    private static String userEndpoint;

    /**
     * Utility to perform HTTP operations in tests.
     * It is a simplistic client HTTP client tailored for testing.
     * Injected by Spring Boot.
     */
    private final RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());

    /**
     * Port used by the test.
     */
    @LocalServerPort
    private int port;

    /**
     * Set the endpoint, could not do it as a field or in a BeforeAll method
     * because the port value is not initialized at this moment.
     */
    @BeforeEach
    public void setup() {
        String baseEndpoint = "http://localhost:" + port + "/api/";
        depositEndpoint = baseEndpoint + "deposit";
        companyEndpoint = baseEndpoint + "company";
        userEndpoint = baseEndpoint + "user";

    }

    /**
     * Test method to create, read, then delete a deposit.
     */
    @Test
    public void endpointTypicalUsageTest() {
        String companyName = "TestCoDeposit";
        double companyBalance = 1000.0;
        String username = "Michel";
        double amount = 250.0;

        String companyDepositEndpoint = depositEndpoint + "/company/" + companyName;
        String userDepositEndpoint = depositEndpoint + "/user/" + username;

        // CREATE
        {
            // Arrange
            Company company = new Company(companyName, companyBalance);
            User user = new User(username);

            restTemplate.postForEntity(companyEndpoint, company, Void.class);
            restTemplate.postForEntity(userEndpoint, user, void.class);

            String mealDeposit = depositEndpoint + "/meal/" + companyName + "/" + username + "/" + amount;
            String giftDeposit = depositEndpoint + "/gift/" + companyName + "/" + username + "/" + amount;

            // Act
            ResponseEntity<Void> responseMeal = restTemplate.postForEntity(mealDeposit, null, Void.class);
            ResponseEntity<Void> responseGift = restTemplate.postForEntity(giftDeposit, null, Void.class);

            // Assert
            assertEquals(HttpStatus.OK, responseMeal.getStatusCode());
            assertEquals(HttpStatus.OK, responseGift.getStatusCode());
        }

        // READ
        {
            // Act
            ResponseEntity<List<Deposit>> companyDeposit = restTemplate.exchange(
                    companyDepositEndpoint,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    }
            );

            ResponseEntity<List<Deposit>> userDeposit = restTemplate.exchange(
                    userDepositEndpoint,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    }
            );

            // Assert
            assertNotNull(companyDeposit);
            assertNotNull(userDeposit);
            assertEquals(HttpStatus.OK, companyDeposit.getStatusCode());
            assertEquals(HttpStatus.OK, userDeposit.getStatusCode());
            assertNotNull(companyDeposit.getBody());
            assertNotNull(userDeposit.getBody());
            assertEquals(2, companyDeposit.getBody().size());
            assertEquals(2, userDeposit.getBody().size());
        }

        // DELETE
        {
            // Arrange
            List<Deposit> userDeposit = restTemplate.exchange(
                    userDepositEndpoint,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<List<Deposit>>() {
                    }
            ).getBody();

            // Act
            if (userDeposit != null) {
                for (Deposit deposit : userDeposit) {
                    restTemplate.delete(depositEndpoint + "/" + deposit.getId(), null, Void.class);
                }
            }

            // Assert
            ResponseEntity<List<Deposit>> newUserDeposit = restTemplate.exchange(
                    userDepositEndpoint,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    }
            );

            ResponseEntity<List<Deposit>> companyDeposit = restTemplate.exchange(
                    companyDepositEndpoint,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    }
            );

            assertEquals(HttpStatus.OK, newUserDeposit.getStatusCode());
            assertNotNull(newUserDeposit.getBody());
            assertEquals(0, newUserDeposit.getBody().size());

            assertEquals(HttpStatus.OK, companyDeposit.getStatusCode());
            assertNotNull(companyDeposit.getBody());
            assertEquals(0, companyDeposit.getBody().size());
        }
    }
}
