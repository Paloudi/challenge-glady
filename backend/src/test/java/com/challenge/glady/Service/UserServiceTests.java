package com.challenge.glady.Service;

import com.challenge.glady.Core.Entity.User;
import com.challenge.glady.Data.Repository.UserRepository;
import com.challenge.glady.Helper.DepositHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Test the user service methods.
 * The database id mocked.
 */
@DataJpaTest
public class UserServiceTests {

    /**
     * Mocked {@link UserRepository} repository.
     */
    @MockBean
    UserRepository userRepository;

    /**
     * Mocked {@link DepositService} service.
     */
    @MockBean
    DepositService depositService;

    /**
     * The {@link DepositHelper} helper.
     */
    DepositHelper depositHelper;

    /**
     * The {@link UserService} service.
     */
    UserService userService;

    /**
     * (Re)initialize the services for each test.
     */
    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        userService = new UserService(userRepository, depositHelper);
    }

    /**
     * Test if the getAllUsers returns a list.
     */
    @Test
    public void getAllUsersTest() {
        // Act
        List<User> result = userService.getAllUsers();

        // Assert
        assertThat(result).isEmpty();
    }

    /**
     * Test the getByName method when the user exists.
     */
    @Test
    public void getByNameTestExisting() {
        // Arrange
        User expectedUser = new User("Michel");
        when(userRepository.findByUsername(expectedUser.getUsername())).thenReturn(expectedUser);

        // Act
        User result = userService.getByName(expectedUser.getUsername());

        // Assert
        assertThat(result).isEqualTo(expectedUser);
    }

    /**
     * Test the getByName method when the user does not exist.
     */
    @Test
    public void getByNameTestNotExisting() {
        // Arrange
        User expectedUser = new User("Michel");
        when(userRepository.findByUsername(expectedUser.getUsername())).thenReturn(null);

        // Act
        User result = userService.getByName(expectedUser.getUsername());

        // Assert
        assertThat(result).isEqualTo(null);
    }

    /**
     * Test the create method when succeeding.
     */
    @Test
    public void createTestSuccess() {
        // Arrange
        User expectedUser = new User("Michel");
        when(userRepository.findByUsername(anyString())).thenReturn(null);
        when(userRepository.save(any(User.class))).thenReturn(expectedUser);

        // Act
        User result = userService.create(expectedUser);

        // Assert
        assertThat(result).isEqualTo(expectedUser);
    }

    /**
     * Test the create method when failing.
     */
    @Test
    public void createTestFailure() {
        // Arrange
        User expectedUser = new User("Michel");
        when(userRepository.findByUsername(anyString())).thenReturn(expectedUser);

        // Act
        User result = userService.create(expectedUser);

        // Assert
        assertThat(result).isEqualTo(null);
    }

    /**
     * Test the create method when succeeding.
     */
    @Test
    public void updateTestSuccess() {
        // Arrange
        User expectedUser = new User("Michel");
        expectedUser.setDeposits(new ArrayList<>());
        when(userRepository.findByUsername(anyString())).thenReturn(expectedUser);
        when(userRepository.saveAndFlush(any(User.class))).thenReturn(expectedUser);

        // Act
        User result = userService.update(expectedUser);

        // Assert
        assertThat(result).isEqualTo(expectedUser);
    }

    /**
     * Test the create method when failing.
     */
    @Test
    public void updateTestFailure() {
        // Arrange
        when(userRepository.findByUsername(anyString())).thenReturn(null);

        // Act
        User result = userService.update(new User("Michel"));

        // Assert
        assertThat(result).isEqualTo(null);
    }

    /**
     * Test the create method when succeeding.
     */
    @Test
    public void deleteTestSuccess() {
        // Arrange
        String username = "Michel";
        when(userRepository.existsByUsername(anyString())).thenReturn(false);

        // Act
        boolean result = userService.delete(username);

        // Assert
        assertThat(result).isEqualTo(true);
    }

    /**
     * Test the create method when failing.
     */
    @Test
    public void deleteTestFailure() {
        // Arrange
        String username = "Michel";
        when(userRepository.existsByUsername(anyString())).thenReturn(true);

        // Act
        boolean result = userService.delete(username);

        // Assert
        assertThat(result).isEqualTo(false);
    }
}
