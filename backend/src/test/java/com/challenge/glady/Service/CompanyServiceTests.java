package com.challenge.glady.Service;

import com.challenge.glady.Core.Entity.Company;
import com.challenge.glady.Data.Repository.CompanyRepository;
import com.challenge.glady.Helper.DepositHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Test the company service methods.
 * The database is mocked.
 */
@DataJpaTest
public class CompanyServiceTests {

    /**
     * The {@link CompanyService} service.
     */
    CompanyService companyService;
    /**
     * Mocked {@link CompanyRepository} repository.
     */
    @MockBean
    private CompanyRepository companyRepository;
    /**
     * The {@link DepositHelper} helper.
     */
    @MockBean
    private DepositHelper depositHelper;

    /**
     * (Re)initialize the services for each test.
     */
    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        companyService = new CompanyService(companyRepository, depositHelper);
    }

    /**
     * Test if the getAllCompanies returns a list.
     */
    @Test
    public void getAllCompaniesTest() {
        // Act
        List<Company> result = companyService.getAllCompanies();

        // Assert
        assertThat(result).isEmpty();
    }

    /**
     * Test the getByName method when the company exists.
     */
    @Test
    public void getByNameTestExisting() {
        // Arrange
        Company expectedCompany = new Company("A", 100.0);
        when(companyRepository.findByName(expectedCompany.getName())).thenReturn(expectedCompany);

        // Act
        Company result = companyService.getByName(expectedCompany.getName());

        // Assert
        assertThat(result).isEqualTo(expectedCompany);
    }

    /**
     * Test the getByName method when the company does not exist.
     */
    @Test
    public void getByNameTestNonExisting() {
        // Arrange
        Company expectedCompany = new Company("A", 100.0);
        when(companyRepository.findByName(expectedCompany.getName())).thenReturn(null);

        // Act
        Company result = companyService.getByName(expectedCompany.getName());

        // Assert
        assertThat(result).isEqualTo(null);
    }

    /**
     * Test the checkCompanyBalance method when succeeding.
     */
    @Test
    public void checkCompanyBalanceTestSuccess() {
        // Arrange
        double amount = 75.0;
        Company company = new Company("A", amount * 2);

        // Act
        boolean result = companyService.checkCompanyBalance(company, amount);

        // Assert
        assertThat(result).isTrue();
    }

    /**
     * Test the checkCompanyBalance method when failing.
     */
    @Test
    public void checkCompanyBalanceTestFailure() {
        // Arrange
        double amount = 75.0;
        Company company = new Company("A", amount / 2);

        // Act
        boolean result = companyService.checkCompanyBalance(company, amount);

        // Assert
        assertThat(result).isFalse();
    }

    /**
     * Test the checkCompanyBalance method when company is null.
     */
    @Test
    public void checkCompanyBalanceTestCompanyIsNull() {
        // Arrange
        double amount = 75.0;

        // Act & Assert
        Exception exception = assertThrows(
                NullPointerException.class,
                () -> companyService.checkCompanyBalance(null, amount),
                "checkCompanyBalance should have throw an exception");

        assertThat(exception.getMessage()).isEqualTo("Company can't be null");
    }

    /**
     * Test the checkCompanyBalance method when amount is NAN.
     */
    @Test
    public void checkCompanyBalanceTestAmountIsNan() {
        // Arrange
        double amount = 75.0;
        Company company = new Company("A", amount * 2);

        // Act & Assert
        Exception exception = assertThrows(
                NullPointerException.class,
                () -> companyService.checkCompanyBalance(company, Double.NaN),
                "checkCompanyBalance should have throw an exception");

        assertThat(exception.getMessage()).isEqualTo("Amount must be a number");
    }

    /**
     * Test the create method when succeeding.
     */
    @Test
    public void createTestSuccess() {
        // Arrange
        Company expectedCompany = new Company("A", 100.0);
        when(companyRepository.findByName(anyString())).thenReturn(null);
        when(companyRepository.save(any(Company.class))).thenReturn(expectedCompany);

        // Act
        Company result = companyService.create(expectedCompany);

        // Assert
        assertThat(result).isEqualTo(expectedCompany);
    }

    /**
     * Test the create method when failing.
     */
    @Test
    public void createTestFailure() {
        // Arrange
        Company expectedCompany = new Company("A", 100.0);
        when(companyRepository.findByName(anyString())).thenReturn(expectedCompany);

        // Act
        Company result = companyService.create(expectedCompany);

        // Assert
        assertThat(result).isEqualTo(null);
    }

    /**
     * Test the update method when succeeding.
     */
    @Test
    public void updateTestSuccess() {
        // Arrange
        Company expectedCompany = new Company("A", 100.0);
        when(companyRepository.findByName(anyString())).thenReturn(expectedCompany);
        when(companyRepository.saveAndFlush(any(Company.class))).thenReturn(expectedCompany);

        // Act
        Company result = companyService.update(expectedCompany);

        // Assert
        assertThat(result).isEqualTo(expectedCompany);
    }

    /**
     * Test the update method when failing.
     */
    @Test
    public void updateTestFailure() {
        // Arrange
        when(companyRepository.findByName(anyString())).thenReturn(null);

        // Act
        Company result = companyService.update(new Company());

        // Assert
        assertThat(result).isEqualTo(null);
    }

    /**
     * Test the delete method when succeeding.
     */
    @Test
    public void deleteTestSuccess() {
        // Arrange
        when(companyRepository.existsByName(anyString())).thenReturn(false);

        // Act
        boolean result = companyService.delete("");

        // Assert
        assertThat(result).isTrue();
    }

    /**
     * Test the delete method when failing.
     */
    @Test
    public void deleteTestFailure() {
        // Arrange
        when(companyRepository.existsByName(anyString())).thenReturn(true);

        // Act
        boolean result = companyService.delete("");

        // Assert
        assertThat(result).isFalse();
    }

}
