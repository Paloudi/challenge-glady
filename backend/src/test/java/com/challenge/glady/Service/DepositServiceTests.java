package com.challenge.glady.Service;

import com.challenge.glady.Core.Entity.*;
import com.challenge.glady.Core.Enum.DepositType;
import com.challenge.glady.Data.Repository.CompanyRepository;
import com.challenge.glady.Data.Repository.DepositRepository;
import com.challenge.glady.Data.Repository.UserRepository;
import com.challenge.glady.Helper.DepositHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

/**
 * Test the deposit service methods.
 * The database is mocked.
 */
@DataJpaTest
public class DepositServiceTests {

    /**
     * Mocked {@link DepositRepository} repository.
     */
    @MockBean
    DepositRepository depositRepository;

    /**
     * Mocked {@link CompanyRepository} repository.
     */
    @MockBean
    CompanyRepository companyRepository;

    /**
     * Mocked {@link UserRepository} repository.
     */
    @MockBean
    UserRepository userRepository;

    /**
     * The {@link CompanyService} service.
     */
    @MockBean
    CompanyService companyService;

    /**
     * The {@link DepositHelper} helper
     */
    DepositHelper depositHelper;

    /**
     * The {@link UserService} service.
     */
    UserService userService;

    /**
     * The {@link DepositService} service to test.
     */
    DepositService depositService;

    /**
     * (Re)initialize the services for each test.
     */
    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        userService = new UserService(userRepository, depositHelper);
        depositService = new DepositService(depositRepository, companyService, userService);
    }

    /**
     * Test if the getById method return a not null value when succeeded.
     */
    @Test
    public void getByIdTestSuccess() {
        // Arrange
        Deposit expected = new GiftDeposit(0.0, LocalDate.now(), new Company(), new User());
        when(depositRepository.findById(any())).thenReturn(Optional.of(expected));

        // Act
        Deposit result = depositService.getById("1");

        // Assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(expected);
    }

    /**
     * Test if the getById method return null value when failing.
     */
    @Test
    public void getByIdTestFailure() {
        // Arrange
        when(depositRepository.findById(any())).thenReturn(null);

        // Act
        Deposit result = depositService.getById("1");

        // Assert
        assertThat(result).isNull();
    }

    /**
     * Test if the getAllDeposits method return a not null value when succeeded.
     */
    @Test
    public void getAllDepositsTestSuccess() {
        // Act
        List<Deposit> deposits = depositService.getAllDeposits();

        // Assert
        assertThat(deposits).isNotNull();
    }

    /**
     * Test the getAllDepositsOfCompany method succeed.
     */
    @Test
    public void getAllDepositsOfCompanyTestSuccess() {
        // Arrange
        String name = "A";
        Company company = new Company(name, 100.0);
        ArrayList<Deposit> deposits = new ArrayList<>();
        deposits.add(new MealDeposit());
        deposits.add(new GiftDeposit());
        when(companyService.getByName(anyString())).thenReturn(company);
        when(depositRepository.findAllByCompany(company)).thenReturn(deposits);

        // Act
        List<Deposit> result = depositService.getAllDepositsOfCompany(name);

        // Assert
        assertThat(result).isNotNull();
        assertThat(result).hasSize(deposits.size());
    }

    /**
     * Test the getAllDepositsOfCompany method failure.
     */
    @Test
    public void getAllDepositsOfCompanyTestFailure() {
        // Arrange
        String name = "A";
        when(companyService.getByName(anyString())).thenReturn(null);

        // Act
        List<Deposit> result = depositService.getAllDepositsOfCompany(name);

        // Assert
        assertThat(result).isNotNull();
        assertThat(result).hasSize(0);
    }

    /**
     * Test the getAllDepositsOfUser method succeed.
     */
    @Test
    public void getAllDepositsOfUserTestSuccess() {
        // Arrange
        String name = "A";
        User user = new User(name);
        ArrayList<Deposit> deposits = new ArrayList<>();
        deposits.add(new MealDeposit());
        deposits.add(new GiftDeposit());
        when(userService.getByName(anyString())).thenReturn(user);
        when(depositRepository.findAllByUser(user)).thenReturn(deposits);

        // Act
        List<Deposit> result = depositService.getAllDepositsOfUser(name);

        // Assert
        assertThat(result).isNotNull();
        assertThat(result).hasSize(deposits.size());
    }

    /**
     * Test the getAllDepositsOfUser method failure.
     */
    @Test
    public void getAllDepositsOfUserTestFailure() {
        // Arrange
        String name = "A";
        when(companyService.getByName(anyString())).thenReturn(null);

        // Act
        List<Deposit> result = depositService.getAllDepositsOfUser(name);

        // Assert
        assertThat(result).isNotNull();
        assertThat(result).hasSize(0);
    }

    /**
     * Test if the distributeGiftDeposit method succeed with enough balance.
     */
    @Test
    public void distributeGiftDepositTestSuccess() {
        // Arrange
        double balance = 300.0;
        double amount = 150.0;
        Company company = new Company("Glady", balance);
        User user = new User("Michel");
        when(companyService.checkCompanyBalance(company, amount)).thenReturn(true);

        // Act
        boolean result = depositService.distributeDeposit(DepositType.GIFT, company, user, amount);

        // Assert
        assertThat(result).isTrue();
        assertThat(company.getBalance()).isEqualTo(balance - amount);
    }

    /**
     * Test if the distributeGiftDeposit method fail when not enough balance.
     */
    @Test
    public void distributeGiftDepositTestFailure() {
        // Arrange
        double balance = 75.0;
        double amount = 150.0;
        Company company = new Company("Glady", balance);
        User user = new User("Michel");

        // Act
        boolean result = depositService.distributeDeposit(DepositType.GIFT, company, user, amount);

        // Assert
        assertThat(result).isFalse();
        assertThat(company.getBalance()).isEqualTo(balance);
    }

    /**
     * Test the distributedGiftDeposit method behavior when an exception is thrown.
     */
    @Test
    public void distributeGiftDepositTestException() {
        // Arrange
        double amount = 150.0;
        Company company = new Company("Glady", amount);
        User user = new User("Michel");
        CompanyService mockedCompanyService = mock(CompanyService.class);
        depositService = new DepositService(depositRepository, mockedCompanyService, userService);
        when(mockedCompanyService.checkCompanyBalance(any(), anyDouble())).thenThrow(new RuntimeException());

        // Act
        boolean result = depositService.distributeDeposit(DepositType.GIFT, company, user, amount);

        // Assert
        assertThat(result).isFalse();
    }

    /**
     * Test if the distributeMealDeposit method succeed with enough balance.
     */
    @Test
    public void distributeMealDepositTestSuccess() {
        // Arrange
        double amount = 150.0;
        double balance = amount * 2;
        Company company = new Company("Glady", balance);
        User user = new User("Michel");
        when(companyService.checkCompanyBalance(company, amount)).thenReturn(true);

        // Act
        boolean result = depositService.distributeDeposit(DepositType.MEAL, company, user, amount);

        // Assert
        assertThat(result).isTrue();
        assertThat(company.getBalance()).isEqualTo(balance - amount);
    }

    /**
     * Test if the distributeMealDeposit method fail if not enough balance.
     */
    @Test
    public void distributeMealDepositTestFailure() {
        // Arrange
        double balance = 75.0;
        double amount = 150.0;
        Company company = new Company("Glady", balance);
        User user = new User("Michel");

        // Act
        boolean result = depositService.distributeDeposit(DepositType.MEAL, company, user, amount);

        // Assert
        assertThat(result).isFalse();
        assertThat(company.getBalance()).isEqualTo(balance);
    }

    /**
     * Test the distributedMealDeposit method behavior when an exception is thrown.
     */
    @Test
    public void distributeMealDepositTestException() {
        // Arrange
        double amount = 150.0;
        Company company = new Company("Glady", amount);
        User user = new User("Michel");
        when(companyService.checkCompanyBalance(company, amount)).thenThrow(new RuntimeException());

        // Act
        boolean result = depositService.distributeDeposit(DepositType.MEAL, company, user, amount);

        // Assert
        assertThat(result).isFalse();
    }

    /**
     * Test the getValidDepositsForUser method when succeeding.
     */
    @Test
    public void getValidDepositsForUserTestSuccess() {
        // Arrange
        Map<Long, User> inMemoryStorageOfUsers = new HashMap<>();

        // Define behavior for save method
        when(userRepository.save(any(User.class))).thenAnswer((Answer<User>) invocationOnMock -> {
            User user = invocationOnMock.getArgument(0);
            // Simulate auto-generation of ID for simplicity.
            long newId = inMemoryStorageOfUsers.size() + 1;
            user.setId(newId);
            inMemoryStorageOfUsers.put(newId, user);
            return user;
        });

        when(depositRepository.findAllByUser(any(User.class))).thenAnswer((Answer<List<Deposit>>) invocationOnMock -> {
            User user = invocationOnMock.getArgument(0);
            return inMemoryStorageOfUsers.get(user.getId()).
                    getDeposits().stream().filter(deposit -> !deposit.isExpired()).collect(Collectors.toList());
        });

        List<Deposit> input = new ArrayList<>();
        User user = new User("Michel");
        input.add(new GiftDeposit(100.0, LocalDate.now(), new Company(), user));
        input.add(new MealDeposit(200.0, LocalDate.now(), new Company(), user));
        user.setDeposits(input);
        userService.create(user);

        // Act
        List<Deposit> result = depositService.getValidDepositsForUser(user);

        // Assert
        assertThat(result).hasSize(input.size());
    }

    /**
     * Test the getValidDepositsForUser method failure.
     */
    @Test
    public void getValidDepositsForUserTestFailure() {
        // Act
        List<Deposit> result = depositService.getValidDepositsForUser(null);

        // Assert
        assertThat(result).isEmpty();
    }

    /**
     * Test the getValidDepositsForUser method exception catch
     */
    @Test
    public void getValidDepositsForUserTestException() {
        // Arrange
        // Define behavior for save method
        when(depositRepository.findAllByUser(any(User.class))).thenThrow(RuntimeException.class);

        // Act
        List<Deposit> result = depositService.getValidDepositsForUser(new User());

        // Assert
        assertThat(result).isEmpty();
    }

    /**
     * Test the checkDepositDataValidity private method.
     */
    @Test
    public void checkDepositDataValidityTestNullCompany() {
        // Act & Assert
        Exception ex = assertThrows(
                NullPointerException.class,
                () -> depositService.distributeDeposit(DepositType.GIFT, null, null, Double.NaN),
                "Expected checkDepositDataValidity to throw, but it didn't");

        assertThat(ex.getMessage()).isEqualTo("Company can't be null");

    }

    /**
     * Test the checkDepositDataValidity private method.
     */
    @Test
    public void checkDepositDataValidityTestNullUser() {
        // Act & Assert
        Exception ex = assertThrows(
                NullPointerException.class,
                () -> depositService.distributeDeposit(DepositType.GIFT, new Company(), null, Double.NaN),
                "Expected checkDepositDataValidity to throw, but it didn't");

        assertThat(ex.getMessage()).isEqualTo("User can't be null");
    }

    /**
     * Test the checkDepositDataValidity private method.
     */
    @Test
    public void checkDepositDataValidityTestNanAmount() {
        // Act & Assert
        Exception ex = assertThrows(
                NullPointerException.class,
                () -> depositService.distributeDeposit(DepositType.GIFT, new Company(), new User(), Double.NaN),
                "Expected checkDepositDataValidity to throw, but it didn't");

        assertThat(ex.getMessage()).isEqualTo("Amount is not a number");
    }

    /**
     * Test the getUserBalance methods when succeeding.
     */
    @Test
    public void getUserBalanceTestSuccess() {
        // Arrange
        double balance = 300.0;
        User user = new User("Michel");
        List<Deposit> deposits = new ArrayList<>();
        deposits.add(new GiftDeposit(balance, LocalDate.now(), new Company(), user));
        when(depositService.getValidDepositsForUser(any(User.class))).thenReturn(deposits);

        // Act
        double result = depositService.getUserBalance(user);

        // Assert
        assertThat(result).isEqualTo(balance);
    }

    /**
     * Test the getUserBalance methods when empty.
     */
    @Test
    public void getUserBalanceTestEmpty() {
        // Arrange
        User user = new User("Michel");
        when(depositService.getValidDepositsForUser(any(User.class))).thenReturn(new ArrayList<>());

        // Act
        double result = depositService.getUserBalance(user);

        // Assert
        assertThat(result).isEqualTo(0.0);
    }

    /**
     * Test the createMealDeposit methods when succeeding.
     */
    @Test
    public void createMealDepositTestSuccess() {
        // Arrange
        String companyName = "A";
        String username = "Michel";
        double amount = 50.0;
        Company company = new Company(companyName, 100.0);
        User user = new User(username);
        when(companyService.getByName(anyString())).thenReturn(company);
        when(companyService.checkCompanyBalance(any(Company.class), anyDouble())).thenReturn(true);
        when(userService.getByName(anyString())).thenReturn(user);

        // Act
        boolean result = depositService.createMealDeposit(companyName, username, amount);

        // Assert
        assertThat(result).isTrue();
    }

    /**
     * Test the createMealDeposit method when failing.
     */
    @Test
    public void createMealDepositTestFailure() {
        // Arrange
        String companyName = "A";
        String username = "Michel";
        double amount = 50.0;
        Company company = new Company(companyName, 100.0);
        User user = new User(username);
        when(companyService.getByName(anyString())).thenReturn(company);
        when(companyService.checkCompanyBalance(any(Company.class), anyDouble())).thenReturn(false);
        when(userService.getByName(anyString())).thenReturn(user);

        // Act
        boolean result = depositService.createMealDeposit(companyName, username, amount);

        // Assert
        assertThat(result).isFalse();
    }

    /**
     * Test the createGiftDeposit method when succeeding.
     */
    @Test
    public void createGiftDepositTestSuccess() {
        // Arrange
        String companyName = "A";
        String username = "Michel";
        double amount = 50.0;
        Company company = new Company(companyName, 100.0);
        User user = new User(username);
        when(companyService.getByName(anyString())).thenReturn(company);
        when(companyService.checkCompanyBalance(any(Company.class), anyDouble())).thenReturn(true);
        when(userService.getByName(anyString())).thenReturn(user);

        // Act
        boolean result = depositService.createGiftDeposit(companyName, username, amount);

        // Assert
        assertThat(result).isTrue();
    }

    /**
     * Test the createGiftDeposit method when failing.
     */
    @Test
    public void createGiftDepositTestFailure() {
        // Arrange
        String companyName = "A";
        String username = "Michel";
        double amount = 50.0;
        Company company = new Company(companyName, 100.0);
        User user = new User(username);
        when(companyService.getByName(anyString())).thenReturn(company);
        when(companyService.checkCompanyBalance(any(Company.class), anyDouble())).thenReturn(false);
        when(userService.getByName(anyString())).thenReturn(user);

        // Act
        boolean result = depositService.createGiftDeposit(companyName, username, amount);

        // Assert
        assertThat(result).isFalse();
    }

    /**
     * Test the update method when succeeding.
     */
    @Test
    public void updateTestSuccess() {
        // Arrange
        Deposit deposit = new GiftDeposit(100.0, LocalDate.now(), new Company(), new User());
        when(depositRepository.findDepositByTypeAndCompanyAndUserAndDistributionDate(
                any(), any(), any(), any())).thenReturn(deposit);
        when(depositRepository.save(any())).thenReturn(deposit);

        // Act
        Deposit result = depositService.update(deposit);

        // Assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(deposit);
    }

    /**
     * Test the update method when failing.
     */
    @Test
    public void updateTestFailure() {
        // Arrange
        Deposit deposit = new GiftDeposit(100.0, LocalDate.now(), new Company(), new User());
        when(depositRepository.findDepositByTypeAndCompanyAndUserAndDistributionDate(
                any(), any(), any(), any())).thenReturn(null);

        // Act
        Deposit result = depositService.update(deposit);

        // Assert
        assertThat(result).isNull();
    }

    /**
     * Test the delete method when succeeding.
     */
    @Test
    public void deleteTestSuccess() {
        // Act
        boolean result = depositService.delete("0");

        // Assert
        assertTrue(result);
    }

    /**
     * Test the delete method when failing.
     */
    @Test
    public void deleteTestFailure() {
        // Arrange
        doThrow(RuntimeException.class).when(depositRepository).deleteById(anyLong());

        // Act
        boolean result = depositService.delete("0");

        // Assert
        assertFalse(result);
    }
}
