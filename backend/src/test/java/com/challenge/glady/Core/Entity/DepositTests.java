package com.challenge.glady.Core.Entity;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for {@link DepositTests}, we're testing the entity itself and not the repository.
 */
@DataJpaTest
public class DepositTests {

    /**
     * The tested object for each test.
     * We are using a GiftDeposit because Deposit is abstract.
     */
    private GiftDeposit giftDeposit;

    /**
     * Called before each test, reset the test object state.
     */
    @BeforeEach
    public void setup() {
        giftDeposit = new GiftDeposit();
    }

    /**
     * Test the setters to see if the deposit fields are updated properly.
     */
    @Test
    public void SettersTest() {
        // Arrange
        String companyName = "CompanyTest";
        double balance = 100.0;
        double amount = 75.0;
        long id = 2L;
        LocalDate date = LocalDate.EPOCH;
        Company company = new Company(companyName, balance);

        // Act
        giftDeposit.setId(id);
        giftDeposit.setAmount(amount);
        giftDeposit.setDistributionDate(date);
        giftDeposit.setCompany(company);

        // Assert
        assertThat(giftDeposit.getId()).isEqualTo(id);
        assertThat(giftDeposit.getAmount()).isEqualTo(amount);
        assertThat(giftDeposit.getDistributionDate()).isEqualTo(date);
        assertThat(giftDeposit.getCompany()).isEqualTo(company);
   }
}
