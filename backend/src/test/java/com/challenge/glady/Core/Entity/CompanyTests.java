package com.challenge.glady.Core.Entity;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for {@link Company}, we're testing the entity itself and not the repository.
 */
@DataJpaTest
public class CompanyTests {

    /**
     * The tested object for each test.
     */
    private Company company;

    /**
     * Called before each test, reset the test object state.
     */
    @BeforeEach
    public void setup() {
        company = new Company();
    }

    /**
     * Test the NoArgs constructor behavior.
     */
    @Test
    public void CompanyEmptyConstructorTest() {
        // Assert
        assertThat(company.getBalance()).isEqualTo(0.0);
        assertThat(company.getName()).isNull();
        assertThat(company.getDeposits()).isEmpty();
    }

    /**
     * Test the "name, balance" constructor behavior.
     */
    @Test
    public void CompanyConstructorTest() {
        // Arrange
        String companyName = "companyName";
        double balance = 100.0;

        // Act
        company = new Company(companyName, balance);

        // Assert
        assertThat(company.getBalance()).isEqualTo(balance);
        assertThat(company.getName()).isEqualTo(companyName);
        assertThat(company.getDeposits()).isEmpty();
    }

    /**
     * Test the AllArgs constructor behavior.
     */
    @Test
    public void CompanyAllArgsConstructorTest() {
        // Arrange
        long id = 1L;
        String companyName = "companyName";
        double balance = 100.0;
        List<Deposit> list = new ArrayList<>();
        list.add(new MealDeposit());
        list.add(new GiftDeposit());

        // Act
        company = new Company(id, companyName, balance, list);

        // Assert
        assertThat(company.getId()).isEqualTo(id);
        assertThat(company.getName()).isEqualTo(companyName);
        assertThat(company.getBalance()).isEqualTo(balance);
        assertThat(company.getDeposits()).hasSize(list.size());
    }

    /**
     * Test the setters' behavior.
     */
    @Test
    public void CompanySettersTest() {
        // Arrange
        double balance = 50.0;
        String companyName = "Glady";
        long id = 2L;
        List<Deposit> list = new ArrayList<>();
        list.add(new GiftDeposit());

        // Act
        company.setId(id);
        company.setName(companyName);
        company.setBalance(balance);
        company.setDeposits(list);

        // Assert
        assertThat(company.getId()).isEqualTo(id);
        assertThat(company.getName()).isEqualTo(companyName);
        assertThat(company.getBalance()).isEqualTo(balance);
        assertThat(company.getDeposits()).hasSize(list.size());
    }
}
