package com.challenge.glady.Core.Entity;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for {@link GiftDeposit}, we're testing the entity itself and not the repository.
 */
@DataJpaTest
public class GiftDepositTests {

    /**
     * The tested object for each test.
     */
    private GiftDeposit giftDeposit;

    /**
     * Called before each test, reset the test object state.
     */
    @BeforeEach
    public void setup() {
        giftDeposit = new GiftDeposit();
    }

    /**
     * Test the NoArgs constructor behavior
     */
    @Test
    public void NoArgsConstructorTest() {
        // Assert
        assertThat(giftDeposit.getAmount()).isEqualTo(0.0);
        assertThat(giftDeposit.getDistributionDate()).isNull();
        assertThat(giftDeposit.getCompany()).isNull();
    }

    /**
     * Test if the computation logic for the expiration date is right.
     */
    @Test
    public void ExpirationDateTest() {
        // Arrange
        LocalDate distributionDate = LocalDate.of(2023, 10, 10);
        giftDeposit.setDistributionDate(distributionDate);

        // Act
        LocalDate expectedExpirationDate = distributionDate.plusDays(365);
        LocalDate actualExpirationDate = giftDeposit.computeExpirationDate();

        // Assert
        assertThat(actualExpirationDate).isEqualTo(expectedExpirationDate);
    }

    /**
     * Test if the isExpired method returns true when a gift is expired.
     */
    @Test
    public void IsExpiredTestWhenExpired() {
        // Arrange
        LocalDate distributionDate = LocalDate.of(1980, 1, 1);
        giftDeposit.setDistributionDate(distributionDate);

        // Act
        boolean result = giftDeposit.isExpired();

        // Assert
        // Assuming the test is run after 1981...
        assertThat(result).isTrue();
    }

    /**
     * Test if the isExpired method returns false when a gift is not expired.
     */
    @Test
    public void IsExpiredTestWhenValid() {
        // Arrange
        LocalDate distributionDate = LocalDate.of(2100, 1, 1);
        giftDeposit.setDistributionDate(distributionDate);

        // Act
        boolean result = giftDeposit.isExpired();

        // Assert
        // Assuming the test is run before 2101...
        assertThat(result).isFalse();
    }
}
