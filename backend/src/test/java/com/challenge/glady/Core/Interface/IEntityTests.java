package com.challenge.glady.Core.Interface;

import com.challenge.glady.Data.Repository.CompanyRepository;
import com.challenge.glady.Service.CompanyService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for {@link IEntity}, we're testing the entity itself and not the repository.
 */
@DataJpaTest
public class IEntityTests {

    @MockBean
    CompanyRepository mockedRepository;

    /**
     * Test service.
     */
    CompanyService companyService = new CompanyService(mockedRepository, null);

    /**
     * Test if the logger of IService can be accessed from its children properly.
     */
    @Test
    public void LoggerTest() {
        // Act
        Logger logger = companyService.getLogger();

        // Assert
        assertThat(logger.getName()).isEqualTo(companyService.getClass().getName());
    }
}
