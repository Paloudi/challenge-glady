package com.challenge.glady.Core.Entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for {@link User}, we're testing the entity itself and not the repository.
 */
@DataJpaTest
public class UserTests {

    /**
     * The tested object for each test.
     */
    private User user;

    /**
     * Called before each test, reset the test object state.
     */
    @BeforeEach
    public void setup() {
        user = new User();
    }

    /**
     * Test the NoArgs constructor behavior.
     */
    @Test
    public void UserEmptyConstructorTest() {
        // Assert
        assertThat(user.getUsername()).isNull();
    }

    /**
     * Test the "username" constructor behavior.
     */
    @Test
    public void UserConstructorTest() {
        // Arrange
        String username = "Michel";

        // Act
        user = new User(username);

        // Assert
        assertThat(user.getUsername()).isEqualTo(username);
    }

    /**
     * Test the AllArgs constructor behavior.
     */
    @Test
    public void UserAllArgsConstructorTest() {
        // Arrange
        long id = 1L;
        String username = "Michel";

        // Act
        user = new User(id, username, new ArrayList<>());

        // Assert
        assertThat(user.getId()).isEqualTo(id);
        assertThat(user.getUsername()).isEqualTo(username);
        assertThat(user.getDeposits()).isEmpty();
    }
}
