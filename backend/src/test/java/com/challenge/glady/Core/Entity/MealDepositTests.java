package com.challenge.glady.Core.Entity;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for {@link MealDeposit}, we're testing the entity itself and not the repository.
 */
@DataJpaTest
public class MealDepositTests {

    /**
     * The tested object for each test.
     */
    private MealDeposit mealDeposit;

    /**
     * Called before each test, reset the test object state.
     */
    @BeforeEach
    public void setup() {
        mealDeposit = new MealDeposit();
    }

    /**
     * Test the NoArgs constructor behavior
     */
    @Test
    public void testNoArgsConstructor() {
        // Assert
        assertThat(mealDeposit.getAmount()).isEqualTo(0.0);
        assertThat(mealDeposit.getDistributionDate()).isNull();
        assertThat(mealDeposit.getCompany()).isNull();
    }

    /**
     * Test if the computation logic for the expiration date is right.
     */
    @Test
    public void testExpirationDate() {
        // Arrange
        LocalDate distributionDate = LocalDate.of(2022, 10, 10);
        mealDeposit.setDistributionDate(distributionDate);

        // Act
        LocalDate expectedExpirationDate = LocalDate.of(distributionDate.getYear() + 1, 2, 28);
        LocalDate actualExpirationDate = mealDeposit.computeExpirationDate();

        // Assert
        assertThat(actualExpirationDate).isEqualTo(expectedExpirationDate);
    }

    @Test
    public void testExpirationDateWhenLeapYear() {
        // Arrange
        LocalDate distributionDate = LocalDate.of(1999, 10, 10);
        mealDeposit.setDistributionDate(distributionDate);

        // Act
        LocalDate expectedExpirationDate = LocalDate.of(distributionDate.getYear() + 1, 2, 29);
        LocalDate actualExpirationDate = mealDeposit.computeExpirationDate();

        // Assert
        assertThat(actualExpirationDate).isEqualTo(expectedExpirationDate);
    }

    /**
     * Test if the isExpired method returns true when a gift is expired.
     */
    @Test
    public void testIsExpiredWhenExpired() {
        // Arrange
        LocalDate distributionDate = LocalDate.of(1980, 1, 1);
        mealDeposit.setDistributionDate(distributionDate);

        // Act
        boolean result = mealDeposit.isExpired();

        // Assert
        // Assuming the test is run after 1981...
        assertThat(result).isTrue();
    }

    /**
     * Test if the isExpired method returns false when a gift is not expired.
     */
    @Test
    public void testIsExpiredWhenValid() {
        // Arrange
        LocalDate distributionDate = LocalDate.of(2100, 1, 1);
        mealDeposit.setDistributionDate(distributionDate);

        // Act
        boolean result = mealDeposit.isExpired();

        // Assert
        // Assuming the test is run before 2101...
        assertThat(result).isFalse();
    }
}
