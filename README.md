# Hello Glady!
Hope you're doing fine.  
 I'm hijacking your README to add some information to what I've done.

 I did the [Backend challenge](https://gitlab.com/wedoogift-jobs/challenge/-/tree/master/backend) in Java 21 using a Spring boot stack. It was my first time in a long time and my first time on a 3+ version. It was fun playing with a framework like this after doing low level C++ for the past few weeks.  

Your challenge was on Gitlab so I did it on Gitlab too even if I'm not familiar with the platform at all. I created 2 user stories as issues to mimick how I usually work in Azure DevOps but I could not reproduce all of the links between elements available in Azure.  

I'm not gonna squash the history or delete my branch (I deleted the first one out of habit, oops!) so you can have the full history ( and my fails ): ) but in a real environment, I would squash the history when mergin my feature branch into dev (I'm using gitflow) then delete the branch.  

I configured the repository with merge rules and protected branch to represent how I work habitually. This means no push on ~~master~~ main and dev, only pull/merge requests on them.  

These requests also need to have no issues to be able to be completed to. (Pipeline OK, no open comments, no merge conflict, ...).

The project configuration is not production ready, It's not an oversight. Furthemore, There is no constraint in the challenge explanation so I did not add them myself (positive only balance, max length names, maximum of active deposit per user, ...), this is not an othersight neither.

For the stack, It's a Spring boot project with Lombok to reduce boilerplate code, H2 for a quick'n'dirty database and apache HTTPClient because HTTP method PATCH was not supported out of the box when I did the controllers tests.

Code is documented + commented and is readable, did not used anything to fancy like ternary operation because I know not everyone like them.

You have an API documentation available in the project wiki, [quick access over here](https://gitlab.com/Paloudi/challenge-glady/-/wikis/home).  

The .git folder is available here: https://gofile.io/d/vrn2Sq

# jobs
If you want to join our engineering team, you will most likely have to complete one of our challenges:

* [Backend challenge](https://gitlab.com/wedoogift-jobs/challenge/-/tree/master/backend)
* [Frontend challenge](https://gitlab.com/wedoogift-jobs/challenge/-/tree/master/frontend)
* [DevOps challenge](https://gitlab.com/wedoogift-jobs/challenge/-/tree/master/devops)
* [React challenge](https://gitlab.com/wedoogift-jobs/challenge/-/tree/master/react)
* [Data challenge](https://gitlab.com/wedoogift-jobs/challenge/-/tree/master/data)

In order to know more about opened positions on [Glady](https://www.glady.com/), you can check out this [page](https://jobs.glady.com).

## Sending your result
Please use git to do the test. You can clone the project, work on it, and send us back the link (Gitlab / Github / Bitbucket ...).
